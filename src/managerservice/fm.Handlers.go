package managerservice

import (
	"errors"
	"fmt"
	api "server/API"
	apiC "server/API/commands"
	"server/models"
	"server/postgres"
	"time"
)

func (fm *FoodManager) NewOrder(data *apiC.ComNewOrderData) (*models.MemOrder, int) {
	is_modules_ready := fm.HardM.GetModuleId()
	if is_modules_ready == 0 {
		fmLogger.LogError(errors.New(fmt.Sprintf("No modules connected, order (%d)", data.PartnerOrderId)))
		return nil, api.ErrorNoModulesConnected
	}

	bowlArr, errcode := fm.NOCheckOrder(data)
	if errcode != 0 {
		return nil, errcode
	}

	ord, _ := postgres.Db.InsertOrder(fm.SessionDayId, data.PartnerOrderId, bowlArr, data.Total)

	memord, _ := fm.NOMemOrder(ord, bowlArr)

	fm.HardM.Mutex.Lock()
	fm.HardM.Orders.PushBack(memord)
	fm.HardM.Mutex.Unlock()
	for opos, b := range memord.Bowls {
		id := fm.HardM.GetModuleId()
		fm.HardM.Mutex.Lock()
		fm.HardM.Modules[id].Bowls.PushBack(b)
		fm.HardM.Mutex.Unlock()
		b.ModuleId = id
		b.QueuePos = fm.HardM.Modules[id].Bowls.Len()
		b.OrderPos = opos + 1
	}
	return memord, 0
}

func (fm *FoodManager) NOCheckOrder(data *apiC.ComNewOrderData) ([]*models.DbBowl, int) {
	bowlArr := []*models.DbBowl{}
	reqItems := map[int]int{}

	for _, tmpBowl := range data.Bowls {

		var b *models.DbBowl
		var errCode int
		if !tmpBowl.CustomFlag {
			b, errCode = postgres.Db.GetBowl(tmpBowl.Code)
			if errCode != 0 {
				return nil, api.ErrorUnknownRecipe
			}
		} else {
			b = &models.DbBowl{
				Name:        tmpBowl.Name,
				Price:       tmpBowl.Price,
				CookingTime: 60,
				Type:        tmpBowl.Type,
			}
		}
		if tmpBowl.CustomFlag || tmpBowl.Modify {
			orderedItems := fm.NOOrganizeItems(tmpBowl.Items)
			b.Items = orderedItems
		}

		b.Modify = tmpBowl.Modify
		b.CustomFlag = tmpBowl.CustomFlag
		b.ShowInMenu = tmpBowl.ShowInMenu

		for i := 0; i < tmpBowl.Count; i++ {
			bowlArr = append(bowlArr, b)
		}
		for _, i := range b.Items {
			reqItems[i.ItemCode] += i.Count * tmpBowl.Count
		}
	}
	for code, count := range reqItems {
		available, errCode := postgres.Db.GetAvailableItem(code)
		if errCode != 0 {
			return nil, api.ErrorInternal
		}
		if available < count {
			return nil, api.ErrorNotEnoughItems
		}
	}
	return bowlArr, 0
}

func (fm *FoodManager) NOOrganizeItems(items []models.DbRecipeItem) []models.DbRecipeItem {
	var (
		arr      []models.DbRecipeItem
		BaseArr  []models.DbRecipeItem
		VegArr   []models.DbRecipeItem
		ProtArr  []models.DbRecipeItem
		DressArr []models.DbRecipeItem
		ToppArr  []models.DbRecipeItem
	)
	for _, i := range items {
		dbI, err := postgres.Db.GetItemByCode(i.ItemCode)
		if err == nil {
			i.ThrowSpeed = dbI.ThrowSpeed
			switch dbI.Type {
			case "base":
				BaseArr = append(BaseArr, i)
			case "veggies":
				VegArr = append(VegArr, i)
			case "dressings":
				DressArr = append(DressArr, i)
			case "protein":
				ProtArr = append(ProtArr, i)
			case "topping":
				ToppArr = append(ToppArr, i)
			}
		}
	}
	arr = append(arr, BaseArr...)
	arr = append(arr, VegArr...)
	arr = append(arr, ProtArr...)
	arr = append(arr, DressArr...)
	arr = append(arr, ToppArr...)
	return arr
}

func (fm *FoodManager) NOMemOrder(ord *models.DbOrder, bowlArr []*models.DbBowl) (*models.MemOrder, error) {
	memord := &models.MemOrder{
		Id:        ord.Id,
		DayId:     ord.DayId,
		PartnerId: ord.PartnerId,
		Status:    models.OrderStatusPending,
		OrderTime: ord.OrderTime,
	}

	for iter, b := range bowlArr {
		membowl := &models.MemBowl{
			Id:          ord.Bowls[iter].StructId,
			Code:        b.Code,
			Name:        b.Name,
			Status:      models.OrderStatusPending,
			Order:       memord,
			CookingTime: b.CookingTime,
		}
		for _, i := range b.Items {
			for iter := 0; iter < i.Count; iter++ {
				memitem := &models.MemItem{
					Code:       i.ItemCode,
					Status:     models.OrderStatusPending,
					ThrowSpeed: i.ThrowSpeed,
				}
				membowl.Items = append(membowl.Items, memitem)
			}
			postgres.Db.ReserveItems(i.ItemCode, i.Count)
		}
		memord.Bowls = append(memord.Bowls, membowl)
	}
	return memord, nil
}

func (fm *FoodManager) LoadStorage(data *apiC.ComLoadStorageData) (string, int) {
	errCode := postgres.Db.LoadTray(data.TrayId, data.ItemCode, data.Amount, data.ProductionTime)
	if errCode != 0 {
		return "", errCode
	}
	return data.TrayId, 0
}

func (fm *FoodManager) ClearTray(data *apiC.ComClearTrayData) (string, int) {
	errCode := postgres.Db.ClearTray(data.Tray)
	if errCode != 0 {
		return "", errCode
	}
	return data.Tray, 0
}

func (fm *FoodManager) ChangeModule(data *apiC.ComChangeModuleData) int {

	el := fm.HardM.GetElementByPos(fm.HardM.Modules[data.OldModule].Bowls, data.OldPos)
	if el == nil {
		fmLogger.LogInfo(fmt.Sprintf("No element on module(%d) pos(%d)", data.OldModule, data.OldPos))
		return api.ErrorInternal
	}
	prevel := fm.HardM.GetElementByPos(fm.HardM.Modules[data.NewModule].Bowls, data.NewPos-1)
	if prevel == nil && data.NewPos != 1 {
		fmLogger.LogInfo(fmt.Sprintf("No element on module(%d) pos(%d)", data.NewModule, data.NewPos-1))
		return api.ErrorInternal
	}

	b := el.Value.(*models.MemBowl)
	if fm.HardM.Modules[data.OldModule].Enable {
		fmLogger.LogInfo(fmt.Sprintf("Wrong status element  module(%d) pos(%d) status(%s)", b.ModuleId, b.QueuePos, b.Status))
		return api.ErrorInternal
	}
	err := fm.HardM.RemoveBowl(b)
	if err != nil {
		fmLogger.LogInfo(fmt.Sprintf("Wrong insert element to new module(%d) pos(%d)", b.ModuleId, b.QueuePos))
		return api.ErrorInternal
	}

	if data.NewPos != 1 {
		prevBowl := prevel.Value.(*models.MemBowl)
		if prevBowl.Order.DayId != data.Order {
			fmLogger.LogInfo(fmt.Sprintf("Wrong status element module(%d) pos(%d) status(%s)", prevBowl.ModuleId, prevBowl.QueuePos, prevBowl.Status))
			return api.ErrorWrongChangeModuleData
		}
		err = fm.HardM.InsertBowlAfter(data.NewModule, b, prevel)
		if err != nil {
			fmLogger.LogInfo(fmt.Sprintf("Wrong insert element to new module(%d) pos(%d)", data.NewModule, data.NewPos))
			return api.ErrorInternal
		}
	} else {
		err = fm.HardM.InsertBowlAfter(data.NewModule, b, nil)
		if err != nil {
			fmLogger.LogInfo(fmt.Sprintf("Wrong insert element to new module(%d) pos(%d)", data.NewModule, data.NewPos))
			return api.ErrorInternal
		}
	}
	for _, i := range b.Items {
		i.Status = models.OrderStatusPending
	}
	b.Loaded = 0
	b.Status = models.OrderStatusPending
	return 0
}

func (fm *FoodManager) SetModuleState(data *apiC.ComSetModuleStateData) (int, int) {
	if data.State == true {
		fm.HardM.Modules[data.Module].Enable = true
	}
	if data.State == false {
		fm.HardM.Modules[data.Module].Enable = false
	}
	return data.Module, 0
}

func (fm *FoodManager) OrderChanged(data *apiC.ComOrderChangedData) error {
	return nil
}

func (fm *FoodManager) InitModuleLcd(data *apiC.ComInitModuleLcdData, u *UserManager) int {
	if data.ModuleId > NumModules || data.ModuleId <= 0 {
		return api.ErrorWrongModuleData
	}
	if len(fm.ModulesLcd) > NumLcd {
		return api.ErrorTooMuchLcd
	}

	fm.Mutex.Lock()
	delete(fm.Users, u.Id)
	ml := &ModuleLcd{
		User:     u,
		ModuleId: data.ModuleId,
	}
	fm.ModulesLcd[u.Id] = ml
	fm.Mutex.Unlock()
	fmLogger.LogInfo(fmt.Sprintf("lcd connected to module (%d)", data.ModuleId))
	return 0
}

func (fm *FoodManager) ChangeModuleLcd(data *apiC.ComChangeModuleLcdData, u *UserManager) error {
	if data.NewModuleId > NumModules || data.NewModuleId <= 0 {
		return errors.New(fmt.Sprintf("Wrong module id (%d)", data.NewModuleId))
	}

	if lcd, ok := fm.ModulesLcd[u.Id]; ok {
		lcd.ModuleId = data.NewModuleId
	} else {
		return errors.New(fmt.Sprintf("Wrong command"))
	}

	fmLogger.LogInfo(fmt.Sprintf("lcd connected to module (%d)", data.NewModuleId))
	return nil
}

func (fm *FoodManager) SetTrayBlock(data *apiC.ComSetTrayBlockData) int {
	return postgres.Db.SetTrayState(data.TrayId, data.IsActive)
}

func (fm *FoodManager) DeleteFromMenu(data *apiC.ComDeleteFromMenuData) error {
	return postgres.Db.DeleteBowlFromMenu(data.BowlCode)
}

func (fm *FoodManager) CancelOrder(data *apiC.ComCancelOrderData) int {
	o := &models.MemOrder{}
	for el := fm.HardM.Orders.Front(); el != nil; el = el.Next() {
		if el.Value.(*models.MemOrder).PartnerId == data.PartnerOrderId {
			o = el.Value.(*models.MemOrder)
			break
		}
	}
	if o != nil && o.PartnerId == data.PartnerOrderId {
		reserved := map[int]int{}
		for _, b := range o.Bowls {
			fm.HardM.CanM.SendBowlCancelled(b)
			postgres.Db.SetBowlStatus(b.Id, b.ModuleId, models.OrderStatusClosed)
			if b.Sent {
				for _, i := range b.Items {
					reserved[i.Code]++
				}
			}
			fm.HardM.RemoveBowl(b)
		}
		for code, count := range reserved {
			postgres.Db.ClearBowlReserved(count, code)
		}
		postgres.Db.SetOrderStatus(o.Id, models.OrderStatusCancelled)
		fm.HardM.RemoveOrder(o)
	} else {
		return api.ErrorWrongOrderData
	}
	return 0
}

func (fm *FoodManager) TotalUnload(Com *HttpCom) {
	st, errCode := postgres.Db.GetTrays()
	if errCode != 0 {
		fmLogger.LogStrError(errCode, fmt.Sprintf("Can't unload storage"), Com.User.Ip)
		Com.User.SendHttpCom(fm.ErrorResponse(errCode))
		return
	}
	for _, t := range st {
		fm.Unloading(Com, &t)
	}
}

func (fm *FoodManager) Unloading(Com *HttpCom, tr *models.DbTray) {

	count := 0
	for _, b := range tr.Batches {
		count += len(b.Cartridges)
	}

	if count > 0 {
		for fm.HardM.Unloading {
			time.Sleep(500 * time.Millisecond)
		}

		fm.HardM.Unloading = true
		fm.HardM.CanM.SendUnload(tr.ID, count)
		_ = postgres.Db.ClearTray(tr.ID)

		for fm.HardM.Unloading {
			time.Sleep(500 * time.Millisecond)
		}
		fm.Broadcast <- fm.UpdateTray(tr.ID)
		fm.Broadcast <- fm.UpdateItemsCount()
		fm.Broadcast <- fm.UpdateBowlsCount()
	}
}
