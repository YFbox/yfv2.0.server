package managerservice

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"

	api "server/API"
	apiC "server/API/commands"
	"server/postgres"
	"server/somelogger"
)

var fmLogger *somelogger.Slogger

func NewManager() *FoodManager {
	fmLogger = somelogger.InitSlogger("foodManager")
	umLogger = somelogger.InitSlogger("userManager")

	mu := &sync.Mutex{}
	err := postgres.InitService()
	if err != nil {
		fmLogger.LogFatal(errors.New("Can't connect to database"))
		return nil
	}

	hm := NewHardManager()
	if hm == nil {
		fmLogger.LogFatal(errors.New("Can't init hard manager"))
		return nil
	}
	fm := &FoodManager{
		Mutex:      mu,
		User:       make(chan *UserManager),
		Users:      make(map[string]*UserManager),
		ModulesLcd: make(map[string]*ModuleLcd),
		Unreg:      make(chan *UserManager),
		Com:        make(chan *HttpCom),
		Broadcast:  make(chan *HttpCom),
		LcdUpdate:  make(chan *HttpCom),
		HardM:      hm,
		CloseFlag:  false,
	}

	hm.Manager = fm
	return fm
}

func (fm *FoodManager) Start() {

	go fm.HardM.Start()
	if fm.HardM.EmulatorFlag {
		go fm.HardM.TestServ()
	}
	defer func() {
		fm.HardM.CloseFlag = true
	}()

	id, errCode := postgres.Db.StartDaySession()
	if errCode != 0 {
		fm.CloseFlag = true
		fm.HardM.CloseFlag = true
		fmLogger.LogFatal(errors.New("Can't start day session"))
	}
	fm.SessionDayId = id
	fmLogger.LogInfo("Food manager started")

	go fm.Process()
	go fm.StorageControl()
	//	fm.HardM.CanM.SendConvPump(0)
	for !fm.CloseFlag {
		select {
		case u := <-fm.User:
			if len(fm.Users) < MaxUserCount {
				fmLogger.LogInfo("New user connected (" + "id: " + u.Id + ")")

				fm.Mutex.Lock()
				fm.Users[u.Id] = u
				fm.Mutex.Unlock()

				u.Manager = fm
				u.ManageSending()
			}

		case un := <-fm.Unreg:
			fmLogger.LogInfo("User disconnected (" + "id: " + un.Id + ")")
			un.StopFlag = true
			fm.Mutex.Lock()
			delete(fm.Users, un.Id)
			delete(fm.ModulesLcd, un.Id)
			fm.Mutex.Unlock()

		case com := <-fm.Broadcast:
			for _, u := range fm.Users {
				u.SendHttpCom(com)
			}
			if com.Com == api.ResponseUpdateBowl {
				fmLogger.LogInfo(fmt.Sprintf("Broadcast "+"com (%s), data (%s) ", com.Com, com.Data))
			} else {
				fmLogger.LogInfo(fmt.Sprintf("Broadcast "+"com (%s) ", com.Com))
			}
		case com := <-fm.LcdUpdate:
			for _, m := range fm.ModulesLcd {
				m.User.SendHttpCom(com)
			}
		}
	}
}

func (fm *FoodManager) Process() {
	for !fm.CloseFlag {

		Com := <-fm.Com

		if Com.PartnerKey == api.GLOBAL_PARTNER_KEY {

			switch Com.Com {
			case api.ComNewOrder:

				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComNewOrderData{}
				json.Unmarshal(binary, tmp)

				errCode := fm.VerifyComNewOrderData(tmp)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, fmt.Sprintf("Can not make new order (%d)", tmp.PartnerOrderId), Com.User.Ip)
					Com.User.SendHttpCom(fm.OrderErrorResponse(tmp.PartnerOrderId, errCode))
				} else {
					if !fm.HardM.Unloading {
						ord, errCode := fm.NewOrder(tmp)
						if errCode != 0 {
							fmLogger.LogStrError(errCode, fmt.Sprintf("Can not make new order (%d)", tmp.PartnerOrderId), Com.User.Ip)
							Com.User.SendHttpCom(fm.OrderErrorResponse(tmp.PartnerOrderId, errCode))
						} else {
							fm.Broadcast <- fm.UpdateOrderNumber(ord)
							fm.Broadcast <- fm.UpdateItemsCount()
							fm.Broadcast <- fm.UpdateBowls()
							fm.Broadcast <- fm.UpdateBowlsCount()
							fm.Broadcast <- fm.UpdateOrders()
							fm.HardM.CheckLoad()
						}
					} else {
						Com.User.SendHttpCom(fm.OrderErrorResponse(tmp.PartnerOrderId, api.ErrorDueUnloading))
					}
				}
			case api.ComCancelOrder:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComCancelOrderData{}
				json.Unmarshal(binary, tmp)

				errCode := fm.CancelOrder(tmp)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, fmt.Sprintf("Can not cancel order (%d)", tmp.PartnerOrderId), Com.User.Ip)
					Com.User.SendHttpCom(fm.OrderErrorResponse(tmp.PartnerOrderId, errCode))
				} else {
					fm.Broadcast <- fm.UpdateBowls()
					fm.Broadcast <- fm.UpdateBowlsCount()
					fm.Broadcast <- fm.UpdateOrders()
				}

			case api.ComGetBowls:
				Com.User.SendHttpCom(fm.UpdateBowls())
				Com.User.SendHttpCom(fm.UpdateBowlsCount())

			case api.ComLoadStorage:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComLoadStorageData{}
				json.Unmarshal(binary, tmp)
				tr, errCode := fm.LoadStorage(tmp)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, "Can not load storage", Com.User.Ip)
					Com.User.SendHttpCom(fm.ErrorResponse(errCode))
				} else {
					fm.Broadcast <- fm.UpdateTray(tr)
					fm.Broadcast <- fm.UpdateItemsCount()
					fm.Broadcast <- fm.UpdateBowlsCount()
				}

			case api.ComGetIngredients:
				Com.User.SendHttpCom(fm.UpdateItems())
				Com.User.SendHttpCom(fm.UpdateItemsCount())

			case api.ComGetStorage:
				Com.User.SendHttpCom(fm.UpdateStorage())

			case api.ComClearTray:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComClearTrayData{}
				json.Unmarshal(binary, tmp)

				tr, errCode := fm.ClearTray(tmp)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, "Can not clear tray", Com.User.Ip)
					Com.User.SendHttpCom(fm.ErrorResponse(errCode))
				} else {
					fm.Broadcast <- fm.UpdateTray(tr)
					fm.Broadcast <- fm.UpdateItemsCount()
					fm.Broadcast <- fm.UpdateBowlsCount()
				}

			case api.ComChangeModule:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComChangeModuleData{}
				json.Unmarshal(binary, tmp)

				errCode := fm.VerifyComChangeModuleData(tmp)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, "Can not verify change module data", Com.User.Ip)
					Com.User.SendHttpCom(fm.ErrorResponse(errCode))
				} else {
					err := fm.ChangeModule(tmp)
					if err != 0 {
						fmLogger.LogStrError(errCode, "Can not change module data", Com.User.Ip)
						Com.User.SendHttpCom(fm.ErrorResponse(errCode))
					} else {
						fm.HardM.CheckLoadM(tmp.NewModule)
						fm.Broadcast <- fm.UpdateOrders()
					}
				}

			case api.ComGetActiveOrders:
				Com.User.SendHttpCom(fm.UpdateOrders())

			case api.ComGetModules:
				Com.User.SendHttpCom(fm.UpdateModules())

			case api.ComSetModuleState:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComSetModuleStateData{}
				json.Unmarshal(binary, tmp)

				errCode := fm.VerifyComSetModuleStateData(tmp)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, "Can not verify set module state data", Com.User.Ip)
					Com.User.SendHttpCom(fm.ErrorResponse(errCode))
				}
				id, errCode := fm.SetModuleState(tmp)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, "Can not set module state", Com.User.Ip)
					Com.User.SendHttpCom(fm.ErrorResponse(errCode))
				}
				fm.Broadcast <- fm.UpdateModule(id)

			case api.ComGetHistory:
				Com.User.SendHttpCom(fm.UpdateHistory())

			/*case api.ComOrderChanged:
			binary, _ := json.Marshal(Com.Data)
			tmp := &apiC.ComOrderChangedData{}
			json.Unmarshal(binary, tmp)

			err := fm.OrderChanged(tmp)
			if err != nil {
				fmLogger.LogStrError(errCode, "Can not change module data", Com.User.Ip)
				Com.User.SendHttpCom(fm.ErrorResponse(errCode))
			}
			Com.User.SendHttpCom(fm.UpdateBowlsCount())
			Com.User.SendHttpCom(fm.UpdateItemsCount())
			*/
			case api.ComInitModuleLcd:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComInitModuleLcdData{}
				json.Unmarshal(binary, tmp)

				errCode := fm.InitModuleLcd(tmp, Com.User)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, "Can not init LCD", Com.User.Ip)
					Com.User.SendHttpCom(fm.ErrorResponse(errCode))
				} else {
					Com.User.SendHttpCom(fm.UpdateLcd(fm.HardM.Modules[tmp.ModuleId].CurBowl, tmp.ModuleId))
				}
			case api.ComChangeModuleLcd:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComChangeModuleLcdData{}
				json.Unmarshal(binary, tmp)

				err := fm.ChangeModuleLcd(tmp, Com.User)
				if err != nil {
					fmLogger.LogError(err)
				} else {
					Com.User.SendHttpCom(fm.UpdateLcd(fm.HardM.Modules[tmp.NewModuleId].CurBowl, tmp.NewModuleId))
				}
			case api.ComSetTrayBlock:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComSetTrayBlockData{}
				json.Unmarshal(binary, tmp)

				errCode := fm.SetTrayBlock(tmp)

				if errCode != 0 {
					fmLogger.LogStrError(errCode, fmt.Sprintf("Can't set state(%s) to tray(%s)", tmp.IsActive, tmp.TrayId), Com.User.Ip)
					Com.User.SendHttpCom(fm.ErrorResponse(errCode))
				} else {
					fm.Broadcast <- fm.UpdateTray(tmp.TrayId)
					fm.Broadcast <- fm.UpdateItemsCount()
					fm.Broadcast <- fm.UpdateBowlsCount()
				}
			case api.ComUnloadTray:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComUnloadTrayData{}
				json.Unmarshal(binary, tmp)

				tr, errCode := postgres.Db.GetTray(tmp.TrayId)
				if errCode != 0 {
					fmLogger.LogStrError(errCode, fmt.Sprintf("Can't unload storage"), Com.User.Ip)
					Com.User.SendHttpCom(fm.ErrorResponse(errCode))
					return
				}
				go fm.Unloading(Com, tr)
			case api.ComTotalUnload:
				go fm.TotalUnload(Com)

			case api.ComDeleteFromMenu:
				binary, _ := json.Marshal(Com.Data)
				tmp := &apiC.ComDeleteFromMenuData{}
				json.Unmarshal(binary, tmp)
				fm.DeleteFromMenu(tmp)
				fm.Broadcast <- fm.UpdateBowls()
				fm.Broadcast <- fm.UpdateBowlsCount()
			}
		}
	}
}
