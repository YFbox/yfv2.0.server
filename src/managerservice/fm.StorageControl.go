package managerservice

import (
	"server/postgres"
	"time"
)

func (fm *FoodManager) StorageControl() {
	//go fm.CheckStorage()
	postgres.Db.ClearReserved()
	postgres.Db.UpdateExpired()
}

func (fm *FoodManager) CheckStorage() {
	time.Sleep(time.Minute * 2)
	for !fm.CloseFlag {
		fmLogger.LogInfo("Check Overdue...")
		arr, err := postgres.Db.CheckOverdue()
		if err == nil {
			for tray, count := range arr {
				fm.HardM.CanM.SendManThrow(tray, count)
			}
		}
		fmLogger.LogInfo("Overdue Checked")
		time.Sleep(time.Hour)
	}
}
