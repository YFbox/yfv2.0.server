package managerservice

import (
	"container/list"
	"fmt"
	"server/somelogger"
	"time"

	"github.com/gorilla/websocket"
	uuid "github.com/satori/go.uuid"
)

var umLogger *somelogger.Slogger

func GetUser(Conn *websocket.Conn, ip string) *UserManager {
	u2 := uuid.NewV4()
	return &UserManager{
		Conn:      Conn,
		Id:        u2.String(),
		Ip:        ip,
		StopFlag:  false,
		SendQueue: list.New(),
	}
}

func (u *UserManager) ManageSending() {
	go u.Listen()
	go u.Send()
}

func (u *UserManager) Listen() {
	defer func() {
		if r := recover(); r != nil {
			u.Conn.Close()
			u.Manager.Unreg <- u
		}
	}()
	for !u.StopFlag {
		msg := &HttpCom{}
		err := u.Conn.ReadJSON(msg)
		// if err != nil {
		// 	u.Conn.Close()
		// 	u.Manager.Unreg <- u
		// 	return
		// }
		if err == nil {
			msg.User = u
			if msg.Com != "" { //&& msg.PartnerKey == api.GLOBAL_PARTNER_KEY {
				umLogger.LogInfo(fmt.Sprintf("Recieved massage com(%s) data(%s) from user Ip(%s) ", msg.Com, msg.Data, u.Ip))
				u.Manager.Com <- msg
			}
		}

	}
}

func (u *UserManager) Send() {
	for !u.StopFlag {
		if u.SendQueue.Len() > 0 {
			msg := u.SendQueue.Remove(u.SendQueue.Front()).(HttpCom)
			u.Manager.Mutex.Lock()
			u.Conn.WriteJSON(msg)
			u.Manager.Mutex.Unlock()
			umLogger.LogInfo(fmt.Sprintf("Send massage (%s) to user Ip(%s) ", msg.Com, u.Ip))
		}
		time.Sleep(50 * time.Millisecond)
	}
}

func (u *UserManager) SendData(com string, data interface{}) {
	http := HttpCom{
		Com:  com,
		Data: data,
	}
	u.SendQueue.PushBack(http)
}
func (u *UserManager) SendHttpCom(com *HttpCom) {
	u.SendQueue.PushBack(*com)
}
