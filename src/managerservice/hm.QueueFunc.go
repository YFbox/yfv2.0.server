package managerservice

import (
	"container/list"
	api "server/API"
	"server/models"
)

func (hm *HardManager) GetElementByPos(l *list.List, pos int) *list.Element {
	if pos < 1 {
		return nil
	}
	id := 1
	for el := l.Front(); el != nil; el = el.Next() {
		if id == pos {
			return el
		}
		id++
	}
	return nil
}

func (hm *HardManager) RemoveBowl(b *models.MemBowl) error {
	for el := hm.Modules[b.ModuleId].Bowls.Front(); el != nil; el = el.Next() {
		if el.Value.(*models.MemBowl) == b {
			if hm.Modules[b.ModuleId].CurBowl == b {
				hm.Modules[b.ModuleId].CurBowl = nil
			}
			if hm.Modules[b.ModuleId].NextBowl == b {
				hm.Modules[b.ModuleId].NextBowl = nil
			}
			hm.Modules[b.ModuleId].Bowls.Remove(el)
			hm.UpdateQueuePos(hm.Modules[b.ModuleId].Bowls)
			return nil
		}
	}
	return api.ErrorWrongRemoveBowl
}

func (hm *HardManager) UpdateQueuePos(l *list.List) {
	id := 1
	for el := l.Front(); el != nil; el = el.Next() {
		b := el.Value.(*models.MemBowl)
		b.QueuePos = id
		id++
	}
}

func (hm *HardManager) InsertBowlAfter(module int, b *models.MemBowl, prevBowl *list.Element) error {
	if prevBowl == nil {
		if hm.Modules[module].Bowls.Len() == 0 {
			hm.Modules[module].Bowls.PushBack(b)
		} else {
			return api.ErrorWrongInsertBowl
		}
	}
	hm.Modules[module].Bowls.InsertAfter(b, prevBowl)
	b.ModuleId = module
	hm.UpdateQueuePos(hm.Modules[module].Bowls)
	return nil

}

func (hm *HardManager) RemoveOrder(o *models.MemOrder) error {
	for el := hm.Orders.Front(); el != nil; el = el.Next() {
		if el.Value.(*models.MemOrder) == o {
			hm.Orders.Remove(el)
			return nil
		}
	}
	return api.ErrorWrongRemoveOrder
}
