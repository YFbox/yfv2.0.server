package managerservice

import (
	"container/list"
	"sync"

	"server/canservice"
	"server/models"

	"github.com/gorilla/websocket"
)

type FoodManager struct {
	Mutex        *sync.Mutex
	User         chan *UserManager
	Users        map[string]*UserManager
	ModulesLcd   map[string]*ModuleLcd
	Com          chan *HttpCom
	Broadcast    chan *HttpCom
	LcdUpdate    chan *HttpCom
	Unreg        chan *UserManager
	HardM        *HardManager
	CloseFlag    bool
	SessionDayId int
}

type HttpCom struct {
	PartnerKey string       `json:"partnerKey"`
	Com        string       `json:"com"`
	Data       interface{}  `json:"data"`
	User       *UserManager `json:"-"`
}

var (
	NumLcd       = 4
	MaxUserCount = 20
)

type ModuleLcd struct {
	User     *UserManager
	ModuleId int
}

type UserManager struct {
	Id        string
	Ip        string
	Conn      *websocket.Conn
	Manager   *FoodManager
	SendQueue *list.List
	StopFlag  bool
}

type HardManager struct {
	Mutex        *sync.Mutex
	Manager      *FoodManager
	CanM         *canservice.CanManager
	Modules      map[int]*Module
	Unloading    bool
	ManIsActive  bool
	PumpEnable   bool
	EmulatorFlag bool
	Orders       *list.List
	CloseFlag    bool
}

type Module struct {
	Id        int
	Bowls     *list.List
	CurBowl   *models.MemBowl
	NextBowl  *models.MemBowl
	IsActive  bool
	Enable    bool
	NeedPlate bool
	DishReady bool
	LightMode int
	LcdMode   int
}
