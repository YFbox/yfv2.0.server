package managerservice

import (
	api "server/API"
	apiR "server/API/responses"
	"server/models"
	"server/postgres"
)

func (fm *FoodManager) UpdateOrders() *HttpCom {

	data := apiR.ResponseUpdateOrdersData{}
	for el := fm.HardM.Orders.Front(); el != nil; el = el.Next() {
		tmpord := el.Value.(*models.MemOrder)
		ord := apiR.ROUDOrder{
			Id:        tmpord.PartnerId,
			Status:    tmpord.Status,
			OrderTime: tmpord.OrderTime,
		}
		for _, b := range tmpord.Bowls {
			rb := apiR.ROUDBowl{
				Name:        b.Name,
				Code:        b.Code,
				Status:      b.Status,
				ModuleId:    b.ModuleId,
				QueuePos:    b.QueuePos,
				CookingTime: b.CookingTime,
				ReadyTime:   b.ReadyTime,
			}
			for _, i := range b.Items {
				ri := apiR.ROUDItem{
					Code:   i.Code,
					Status: i.Status,
				}
				rb.Items = append(rb.Items, ri)
			}
			ord.Bowls = append(ord.Bowls, rb)
		}
		data.Orders = append(data.Orders, ord)
	}

	return &HttpCom{
		Com:  api.ResponseUpdateOrders,
		Data: data,
	}
}

func (fm *FoodManager) UpdateBowls() *HttpCom {
	bowls, errCode := postgres.Db.GetBowls()

	if errCode != 0 {
		return fm.ErrorResponse(errCode)
	}
	return &HttpCom{
		Com: api.ResponseUpdateBowls,
		Data: apiR.ResponseUpdateBowlsData{
			Bowls: bowls,
		},
	}
}

func (fm *FoodManager) UpdateBowl(b *models.MemBowl) *HttpCom {
	data := apiR.ResponseUpdateBowlData{
		OrderId:   b.Order.PartnerId,
		Code:      b.Code,
		ModuleId:  b.ModuleId,
		QueuePos:  b.QueuePos,
		OrderPos:  b.OrderPos,
		ReadyTime: b.ReadyTime,
		Status:    b.Status,
	}
	for _, i := range b.Items {
		data.ItemsStatus = append(data.ItemsStatus, i.Status)
	}

	return &HttpCom{
		Com:  api.ResponseUpdateBowl,
		Data: data,
	}
}

func (fm *FoodManager) UpdateBowlsCount() *HttpCom {
	bc, errCode := postgres.Db.GetAvailableBowls()
	if errCode != 0 {
		return fm.ErrorResponse(errCode)
	}
	return &HttpCom{
		Com: api.ResponseUpdateBowlsCount,
		Data: apiR.ResponseUpdateBowlsCountData{
			Bowls: bc,
		},
	}
}

func (fm *FoodManager) UpdateStorage() *HttpCom {
	st, errCode := postgres.Db.GetTrays()
	if errCode != 0 {
		return fm.ErrorResponse(errCode)
	}
	return &HttpCom{
		Com: api.ResponseUpdateStorage,
		Data: apiR.ResponseUpdateStorageData{
			Trays: st,
		},
	}
}

func (fm *FoodManager) UpdateTray(id string) *HttpCom {
	tr, errCode := postgres.Db.GetTray(id)
	if errCode != 0 {
		return fm.ErrorResponse(errCode)
	}
	return &HttpCom{
		Com: api.ResponseUpdateTray,
		Data: apiR.ResponseUpdateTrayData{
			Tray: *tr,
		},
	}
}

func (fm *FoodManager) UpdateModules() *HttpCom {
	mArr := []apiR.ResponseUpdateModuleData{}
	for _, m := range fm.HardM.Modules {
		tmpM := apiR.ResponseUpdateModuleData{
			Id:        m.Id,
			NeedPlate: m.NeedPlate,
			IsActive:  m.IsActive,
			Enable:    m.Enable,
			DishReady: m.DishReady,
		}
		mArr = append(mArr, tmpM)
	}

	return &HttpCom{
		Com: api.ResponseUpdateModules,
		Data: apiR.ResponseUpdateModulesData{
			Modules: mArr,
		},
	}
}

func (fm *FoodManager) UpdateModule(id int) *HttpCom {
	if id < 0 || id > NumModules {
		return fm.ErrorResponse(api.ErrorWrongModuleData)
	}
	m := fm.HardM.Modules[id]
	return &HttpCom{
		Com: api.ResponseUpdateModule,
		Data: apiR.ResponseUpdateModuleData{
			Id:        m.Id,
			NeedPlate: m.NeedPlate,
			IsActive:  m.IsActive,
			Enable:    m.Enable,
			DishReady: m.DishReady,
		},
	}
}

func (fm *FoodManager) UpdateItems() *HttpCom {
	Items, errCode := postgres.Db.GetItems()

	if errCode != 0 {
		return fm.ErrorResponse(errCode)
	}
	return &HttpCom{
		Com: api.ResponseUpdateIngredients,
		Data: apiR.ResponseUpdateIngredientsData{
			Items: Items,
		},
	}
}

func (fm *FoodManager) UpdateItemsCount() *HttpCom {
	ic, errCode := postgres.Db.GetAvailableItems()
	if errCode != 0 {
		fm.ErrorResponse(errCode)
	}
	return &HttpCom{
		Com: api.ResponseUpdateIngredientsCount,
		Data: apiR.ResponseUpdateIngredientsCountData{
			Items: ic,
		},
	}
}

func (fm *FoodManager) UpdateOrderNumber(ord *models.MemOrder) *HttpCom {
	return &HttpCom{
		Com: api.ResponseUpdateOrderNumber,
		Data: apiR.ResponseUpdateOrderNumberData{
			OrderId: ord.PartnerId,
		},
	}
}

func (fm *FoodManager) UpdateHistory() *HttpCom {
	ordArr := []apiR.OrderHistory{}
	his, errCode := postgres.Db.GetOrderHistory()
	if errCode != 0 {
		return fm.ErrorResponse(errCode)
	}

	for _, h := range his {
		o := apiR.OrderHistory{
			OrderId: h.Id,
			DTime:   h.OrderTime,
		}
		for _, b := range h.Bowls {
			vb := apiR.BowlsVerboseState{
				Code:       b.Bowl.Code,
				Name:       b.Bowl.Name,
				Price:      b.Bowl.Price,
				BType:      b.Bowl.Type,
				ModuleId:   b.ModuleId,
				Cartridges: b.Cartridges,
				StartTime:  b.StartTime,
				ReadyTime:  b.ReadyTime,
				ClosedTime: b.CloseTime,
			}
			if b.Bowl.CustomFlag {
				vb.Category = models.BowlCategoryCustom
			} else {
				vb.Category = models.BowlCategoryMenu
			}
			o.Bowls = append(o.Bowls, vb)
		}
		ordArr = append(ordArr, o)
	}
	return &HttpCom{
		Com: api.ResponseUpdateHistory,
		Data: apiR.ResponseUpdateHistoryData{
			Orders: ordArr,
		},
	}
}

func (fm *FoodManager) UpdateLcd(b *models.MemBowl, moduleId int) *HttpCom {
	com := &HttpCom{}
	if b != nil {
		com = &HttpCom{
			Com: api.ResponseUpdateLcd,
			Data: apiR.ResponseUpdateLcdData{
				BowlName:    b.Name,
				Mode:        fm.HardM.Modules[moduleId].LcdMode,
				ModuleId:    b.ModuleId,
				OrderId:     b.Order.PartnerId,
				TimeCooking: b.CookingTime,
				OrderTime:   b.Order.OrderTime,
			}}
	} else {
		com = &HttpCom{
			Com: api.ResponseUpdateLcd,
			Data: apiR.ResponseUpdateLcdData{
				ModuleId: moduleId,
				Mode:     fm.HardM.Modules[moduleId].LcdMode,
			}}
	}
	return com
}

func (fm *FoodManager) OrderErrorResponse(partnerOrderId int, code int) *HttpCom {
	return &HttpCom{
		Com: api.ResponseOrderError,
		Data: apiR.ResponseOrderErrorData{
			OrderId:   partnerOrderId,
			ErrorCode: code,
		}}
}
func (fm *FoodManager) ErrorResponse(code int) *HttpCom {
	return &HttpCom{
		Com: api.ResponseError,
		Data: apiR.ResponseErrorData{
			ErrorCode: code,
		}}
}
