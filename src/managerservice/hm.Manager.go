package managerservice

import (
	"container/list"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	canproto "server/canservice/canProtoConfig"
	"server/models"
	"server/postgres"
	"server/somelogger"

	"server/canservice"
)

var (
	CanPort    = "/dev/ttyACM0" ///origin
	NumModules = 4
)

var hmLogger *somelogger.Slogger

func NewModule(id int) *Module {
	return &Module{
		Id:       id,
		Bowls:    list.New(),
		IsActive: false,
		Enable:   true,
	}
}

func NewHardManager() *HardManager {
	hmLogger = somelogger.InitSlogger("hardManager")
	mu := &sync.Mutex{}
	can := canservice.NewCanManager(CanPort)
	if can == nil {
		hmLogger.LogError(errors.New("Can't init can service"))
		return nil
	}
	h := HardManager{
		Mutex:       mu,
		Orders:      list.New(),
		CanM:        can,
		Modules:     make(map[int]*Module),
		ManIsActive: false,
	}
	if can.ConnErr != nil {
		hmLogger.LogInfo("Connection is not established")
		h.EmulatorFlag = true
	}
	for i := 1; i <= NumModules; i++ {
		h.Modules[i] = NewModule(i)
		if h.Modules[i] == nil {
			return nil
		}
	}
	return &h
}

func (hm *HardManager) Start() {
	hmLogger.LogInfo("Hard manager started")
	defer func() {
		hm.CanM.CloseFlag = true
	}()
	go hm.CanM.Listen()
	go hm.CanM.CanSending()
	go hm.InitStorage()
	go hm.InitModules()
	for !hm.CloseFlag {
		select {
		case m := <-hm.CanM.RxMsg:
			hm.Process(m)
		}
	}

}

func (hm *HardManager) Process(m *canservice.RCanMsg) {
	if m.Dlc > 0 {
		mod_id := int(m.Id>>6) - canproto.CAN_ID_UNPACKER_M0
		if mod_id > 0 && mod_id <= len(hm.Modules) {
			switch m.Id & 0x3F {
			case canproto.UNPACKER_REPLY_CONNECTED:
				if m.Dlc == 1 {
					hm.Mutex.Lock()
					hm.Modules[mod_id].IsActive = true
					hm.Mutex.Unlock()
					hmLogger.LogInfo(fmt.Sprintf("module: %d connected!", mod_id))
				}
			case canproto.UNPACKER_REPLY_PROGRESS:
				if m.Dlc == 2 && hm.Modules[mod_id].CurBowl != nil {
					if hm.Modules[mod_id].CurBowl.Status == models.OrderStatusCooking {
						count_ready := int(m.Data[1])

						if count_ready > 0 && count_ready <= len(hm.Modules[mod_id].CurBowl.Items) {
							for i := 0; i < count_ready; i++ {
								if hm.Modules[mod_id].CurBowl.Items[i].Status == models.OrderStatusCooking {
									hm.Modules[mod_id].CurBowl.Items[i].Status = models.OrderStatusReady
								}
							}
							if count_ready == len(hm.Modules[mod_id].CurBowl.Items) {
								hm.Modules[mod_id].CurBowl.Status = models.OrderStatusReady
								hm.StopBowl(hm.Modules[mod_id].CurBowl)
							} else {
								hm.Manager.Broadcast <- hm.Manager.UpdateBowl(hm.Modules[mod_id].CurBowl)
							}
						}
					}
				}
			case canproto.UNPACKER_REPLY_STATE_LOADED:
				if m.Dlc == 4 && hm.Modules[mod_id].CurBowl != nil {
					var b *models.MemBowl = nil
					bowlId := int(m.Data[2])<<8 + int(m.Data[1])
					if bowlId == hm.Modules[mod_id].CurBowl.Id {
						b = hm.Modules[mod_id].CurBowl
					} else {
						if hm.Modules[mod_id].NextBowl != nil {
							if bowlId == hm.Modules[mod_id].NextBowl.Id {
								b = hm.Modules[mod_id].NextBowl
							}
						}
					}
					if b != nil {
						if b.Status == models.OrderStatusLoading || b.Status == models.OrderStatusCooking {
							count := int(m.Data[3])
							if count > 0 && count <= len(b.Items) {
								for i := 0; i < count; i++ {
									if b.Items[i].Status == models.OrderStatusLoading {
										b.Items[i].Status = models.OrderStatusLoaded
									}
								}
								hm.Manager.Broadcast <- hm.Manager.UpdateBowl(b)
								b.Loaded = count
								if b.Loaded == len(b.Items) {
									hmLogger.LogInfo(fmt.Sprintf("module (%d) bowl code(%d) id(%d) loaded", mod_id, b.Code, b.Id))
									hm.CheckLoadM(mod_id)
								}
							}
						}
					}
				}

			case canproto.UNPACKER_REPLY_STATE_PLATE_OUT:
				if m.Dlc == 1 {
					hm.Modules[mod_id].LightMode = models.LightModeWaiting
					hm.Modules[mod_id].LcdMode = models.LcdModeWaiting

					if hm.Modules[mod_id].CurBowl != nil {
						if hm.Modules[mod_id].CurBowl.Status == models.OrderStatusReady {
							hm.Modules[mod_id].CurBowl.Status = models.OrderStatusClosed
							hm.Manager.Broadcast <- hm.Manager.UpdateBowl(hm.Modules[mod_id].CurBowl)
							hm.CloseBowl(hm.Modules[mod_id].CurBowl)
							hm.CheckLoadM(mod_id)
							if hm.Modules[mod_id].CurBowl != nil {
								hm.Modules[mod_id].LightMode = models.LightModeNeedPlate
								hm.Modules[mod_id].LcdMode = models.LcdModeNeedPlate
							}

						} else {
							hm.Modules[mod_id].LightMode = models.LightModeNeedPlate
							hm.Modules[mod_id].LcdMode = models.LcdModeNeedPlate
						}

					}
					hm.Modules[mod_id].NeedPlate = true
					hm.CanM.SetLightMode(mod_id, uint8(hm.Modules[mod_id].LightMode))
					hm.Manager.LcdUpdate <- hm.Manager.UpdateLcd(nil, mod_id)
					hm.Manager.Broadcast <- hm.Manager.UpdateModule(mod_id)
				}
			case canproto.UNPACKER_REPLY_STATE_PLATE_ON:
				if m.Dlc == 1 {
					if hm.Modules[mod_id].CurBowl != nil {
						switch hm.Modules[mod_id].CurBowl.Status {
						case models.OrderStatusLoading:
							hm.Modules[mod_id].CurBowl.Status = models.OrderStatusCooking
							hm.Manager.Broadcast <- hm.Manager.UpdateBowl(hm.Modules[mod_id].CurBowl)
							hm.StartBowl(hm.Modules[mod_id].CurBowl)
							hm.Modules[mod_id].LightMode = models.LightModeCooking
							hm.Modules[mod_id].LcdMode = models.LcdModeCooking
						case models.OrderStatusCooking:
							hm.Modules[mod_id].LightMode = models.LightModeCooking
							hm.Modules[mod_id].LcdMode = models.LcdModeCooking
						case models.OrderStatusReady:
							hm.Modules[mod_id].LightMode = models.LightModeReady
							hm.Modules[mod_id].LcdMode = models.LcdModeReady
						default:
							hm.Modules[mod_id].LightMode = models.LightModeWaiting
							hm.Modules[mod_id].LcdMode = models.LcdModeWaiting
						}
					} else {
						hm.Modules[mod_id].LightMode = models.LightModeWaiting
						hm.Modules[mod_id].LcdMode = models.LcdModeWaiting
					}
					hm.Modules[mod_id].NeedPlate = false

					hm.CanM.SetLightMode(mod_id, uint8(hm.Modules[mod_id].LightMode))
					hm.Manager.LcdUpdate <- hm.Manager.UpdateLcd(hm.Modules[mod_id].CurBowl, mod_id)
					hm.Manager.Broadcast <- hm.Manager.UpdateModule(mod_id)
				}

			case canproto.UNPACKER_REPLY_STATE_COOKING:
				if m.Dlc == 3 && hm.Modules[mod_id].CurBowl != nil {
					if hm.Modules[mod_id].CurBowl.Status == models.OrderStatusCooking {

						for _, i := range hm.Modules[mod_id].CurBowl.Items {
							if i.Status == models.OrderStatusLoaded {
								i.Status = models.OrderStatusCooking
								break
							}
						}
						hm.Manager.Broadcast <- hm.Manager.UpdateBowl(hm.Modules[mod_id].CurBowl)
					}
				}

			}
		}

		switch m.Id {
		case canproto.MAN_REPLY_CONNECTED:
			if m.Dlc == 1 {
				hm.ManIsActive = true
			}
		case canproto.MAN_REPLY_STATE_LOADING:
			if m.Dlc == 5 {
				mod_id := int(m.Data[1])
				if hm.Modules[mod_id].CurBowl != nil {
					var b *models.MemBowl = nil
					if hm.Modules[mod_id].CurBowl.Loaded != len(hm.Modules[mod_id].CurBowl.Items) {
						b = hm.Modules[mod_id].CurBowl
					} else {
						b = hm.Modules[mod_id].NextBowl
					}
					if b != nil {
						if b.Status == models.OrderStatusPending {
							b.Status = models.OrderStatusLoading
							if b == hm.Modules[mod_id].CurBowl {
								hm.Modules[mod_id].CurBowl.Status = models.OrderStatusCooking
								hm.Modules[b.ModuleId].LightMode = models.LightModeCooking
								hm.Modules[b.ModuleId].LcdMode = models.LcdModeCooking

								if hm.Modules[b.ModuleId].NeedPlate {
									hm.Modules[b.ModuleId].LightMode = models.LightModeNeedPlate
									hm.Modules[b.ModuleId].LcdMode = models.LcdModeNeedPlate
								}

								hm.CanM.SetLightMode(b.ModuleId, uint8(hm.Modules[b.ModuleId].LightMode))
								hm.Manager.LcdUpdate <- hm.Manager.UpdateLcd(b, b.ModuleId)
								hm.Manager.Broadcast <- hm.Manager.UpdateModule(b.ModuleId)
							}

							hmLogger.LogInfo(fmt.Sprintf("module (%d) start loading bowl code(%d) id(%d)!", mod_id, b.Code, b.Id))
						}
						amount := m.Data[4]
						for _, i := range b.Items {
							if i.Status == models.OrderStatusPending {
								i.Status = models.OrderStatusLoading
								amount--
								if amount == 0 {
									break
								}
							}
						}
						hm.Manager.Broadcast <- hm.Manager.UpdateBowl(b)
					}
				}
			}
		case canproto.MAN_REPLY_TRAY_UNLOADED:
			if m.Dlc == 3 {
				hm.Unloading = false
			}
		}
	}
}

func (hm *HardManager) InitModules() {
	// hm.CanM.SendInitModule(1)
	for !hm.CanM.CloseFlag {
		for id, m := range hm.Modules {
			if m.IsActive != true {
				hm.CanM.SendInitModule(id)
				time.Sleep(time.Second)
			}
		}
		time.Sleep(60 * time.Second)
	}
}
func (hm *HardManager) CheckLoad() {
	for num := 1; num <= len(hm.Modules); num++ {
		hm.CheckLoadM(num)
	}
}
func (hm *HardManager) CheckLoadM(id int) {
	if hm.Modules[id].IsActive && hm.Modules[id].Enable && hm.Modules[id].Bowls.Len() > 0 {
		if !hm.PumpEnable {
			hm.PumpEnable = true
			hm.CanM.SendConvPump(1)
		}
		if hm.Modules[id].CurBowl == nil {
			hm.Modules[id].CurBowl = hm.Modules[id].Bowls.Front().Value.(*models.MemBowl)
			hm.LoadBowl(hm.Modules[id].Bowls.Front().Value.(*models.MemBowl))
		}

		if hm.Modules[id].CurBowl.Loaded == len(hm.Modules[id].CurBowl.Items) && hm.Modules[id].NextBowl == nil && hm.Modules[id].Bowls.Len() > 1 {
			hm.Modules[id].NextBowl = hm.Modules[id].Bowls.Front().Next().Value.(*models.MemBowl)
			hm.LoadBowl(hm.Modules[id].NextBowl)
		}
	}
}

func (hm *HardManager) LoadBowl(b *models.MemBowl) {
	for _, i := range b.Items {
		tId, cartr, err := postgres.Db.PickUpItem(i.Code)
		if err != nil {
			hmLogger.LogFatal(errors.New("No items in storage"))
		}
		hmLogger.LogInfo(fmt.Sprintf("Pick up module (%d) bowl code(%d) id(%d) item (%d)", b.ModuleId, b.Code, b.Id, i.Code))

		i.Tray = tId
		err = postgres.Db.SetBowlStruct(int(b.Id), cartr, tId)
		if err != nil {
			hmLogger.LogFatal(errors.New("Database error!"))
		}
		hm.Manager.Broadcast <- hm.Manager.UpdateTray(tId)
	}
	hmLogger.LogInfo(fmt.Sprintf("module (%d) can send bowlId (%d)", b.ModuleId, b.Id))
	hm.CanM.SendBowl(b)
	b.Sent = true
}
func (hm *HardManager) StartBowl(b *models.MemBowl) {

	postgres.Db.SetBowlStatus(int(b.Id), b.ModuleId, models.OrderStatusCooking)

	b.Status = models.OrderStatusCooking
	hm.Manager.Broadcast <- hm.Manager.UpdateBowl(b)

	hmLogger.LogInfo(fmt.Sprintf("module (%d) start cooking bowl code(%d) id(%d)", b.ModuleId, b.Code, b.Id))

	hm.CheckCookingOrder(b.Order)
}
func (hm *HardManager) StopBowl(b *models.MemBowl) {

	hm.Modules[b.ModuleId].LightMode = models.LightModeReady
	hm.Modules[b.ModuleId].LcdMode = models.LcdModeReady

	hm.CanM.SetLightMode(b.ModuleId, uint8(hm.Modules[b.ModuleId].LightMode))
	hm.Manager.LcdUpdate <- hm.Manager.UpdateLcd(b, b.ModuleId)

	rt, _ := postgres.Db.SetBowlStatus(int(b.Id), b.ModuleId, models.OrderStatusReady)
	b.ReadyTime = rt
	hm.Manager.Broadcast <- hm.Manager.UpdateBowl(b)
	hmLogger.LogInfo(fmt.Sprintf("module (%d) finished cooking bowl code(%d) id(%d)", b.ModuleId, b.Code, b.Id))

	hm.Modules[b.ModuleId].DishReady = true
	hm.Manager.Broadcast <- hm.Manager.UpdateModule(b.ModuleId)

	hm.CheckReadyOrders()
}
func (hm *HardManager) CloseBowl(b *models.MemBowl) {
	postgres.Db.SetBowlStatus(int(b.Id), b.ModuleId, models.OrderStatusClosed)

	hmLogger.LogInfo(fmt.Sprintf("module (%d) released bowl code(%d) id(%d)", b.ModuleId, b.Code, b.Id))

	hm.Modules[b.ModuleId].DishReady = false
	hm.Manager.Broadcast <- hm.Manager.UpdateModule(b.ModuleId)

	hm.CheckCloseOrder(b.Order)

	hm.RemoveBowl(b)
	hm.Modules[b.ModuleId].CurBowl = hm.Modules[b.ModuleId].NextBowl
	hm.Modules[b.ModuleId].NextBowl = nil
}

func (hm *HardManager) CheckCookingOrder(o *models.MemOrder) {
	if o.Status != models.OrderStatusCooking {
		o.Status = models.OrderStatusCooking
		hmLogger.LogInfo(fmt.Sprintf("order (%d) start cooking!", o.DayId))

		postgres.Db.SetOrderStatus(o.Id, models.OrderStatusCooking)
	}
}

func (hm *HardManager) CheckReadyOrders() {
	ors := true
	for e := hm.Orders.Front(); e != nil; e = e.Next() {
		or := true
		o := e.Value.(*models.MemOrder)
		for _, b := range o.Bowls {
			if b.Status != models.OrderStatusReady && b.Status != models.OrderStatusClosed {
				or = false
			}
		}
		if or {
			o.Status = models.OrderStatusReady
			hmLogger.LogInfo(fmt.Sprintf("order (%d) ready!", o.DayId))

			postgres.Db.SetOrderStatus(o.Id, models.OrderStatusReady)
		} else {
			ors = false
		}
	}
	if ors {
		hm.CanM.SendConvPump(0)
		hm.PumpEnable = false
	}
}

func (hm *HardManager) CheckCloseOrder(o *models.MemOrder) {
	for _, b := range o.Bowls {
		if b.Status != models.OrderStatusClosed {
			return
		}
	}
	o.Status = models.OrderStatusClosed
	hmLogger.LogInfo(fmt.Sprintf("order (%d) released!", o.DayId))

	postgres.Db.SetOrderStatus(o.Id, models.OrderStatusClosed)
	hm.RemoveOrder(o)
}

func (hm *HardManager) GetModuleId() int {

	flag := true
	minid := 0
	minlen := 0
	for num := 1; num <= len(hm.Modules); num++ {
		l := hm.Modules[num].Bowls.Len()
		if hm.Modules[num].IsActive && hm.Modules[num].Enable {
			if flag {
				minlen = l
				minid = num
				flag = false
			} else {
				if minlen > l {
					minlen = l
					minid = num
				}
			}

		}
	}
	return minid
}

func StrHexToInt(s string) int {
	val := 0
	s = strings.ToLower(s)
	for i := 0; i < len(s); i++ {
		tmpval := 0
		switch s[i] {
		case '0':
			tmpval = 0
		case '1':
			tmpval = 1
		case '2':
			tmpval = 2
		case '3':
			tmpval = 3
		case '4':
			tmpval = 4
		case '5':
			tmpval = 5
		case '6':
			tmpval = 6
		case '7':
			tmpval = 7
		case '8':
			tmpval = 8
		case '9':
			tmpval = 9
		case 'a':
			tmpval = 10
		case 'b':
			tmpval = 11
		case 'c':
			tmpval = 12
		case 'd':
			tmpval = 13
		case 'e':
			tmpval = 14
		case 'f':
			tmpval = 15
		}
		st := len(s) - i - 1
		val += tmpval << (st * 4)
	}
	return val
}

func (hm *HardManager) InitStorage() {
	f := func(posX uint16, trayCol string) {
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 1), posX, models.OFFS_1)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 2), posX, models.OFFS_2)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 3), posX, models.OFFS_3)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 4), posX, models.OFFS_4)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 5), posX, models.OFFS_5)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 6), posX, models.OFFS_6)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 7), posX, models.OFFS_7)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 8), posX, models.OFFS_8)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 9), posX, models.OFFS_9)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 10), posX, models.OFFS_10)
		hm.CanM.SetTrayPos(trayCol+fmt.Sprintf("%02x", 11), posX, models.OFFS_11)
	}
	f(models.OFFS_A, "0A")
	f(models.OFFS_B, "0B")
	f(models.OFFS_C, "0C")
	f(models.OFFS_D, "0D")
}
