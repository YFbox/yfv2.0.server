package managerservice

import (
	"fmt"
	api "server/API"
	apiC "server/API/commands"
	"server/models"
	"server/postgres"
)

func (fm *FoodManager) VerifyComNewOrderData(data *apiC.ComNewOrderData) int {
	if data.Bowls == nil {
		return api.ErrorWrongOrderData
	}
	if len(data.Bowls) == 0 {
		return api.ErrorWrongOrderData
	}
	if data.Total == 0 {
		return api.ErrorWrongOrderData
	}
	for _, tmpBowl := range data.Bowls {
		if tmpBowl.Count == 0 {
			return api.ErrorWrongOrderData
		}
		if !tmpBowl.CustomFlag {
			if ok := postgres.Db.CheckBowlCode(tmpBowl.Code); !ok {
				return api.ErrorUnknownBowl
			}
		} else {
			if tmpBowl.Name == "" {
				return api.ErrorWrongOrderData
			}
			if tmpBowl.Type != models.BowlTypeSalad && tmpBowl.Type != models.BowlTypeSoup {
				return api.ErrorWrongOrderData
			}
		}
		if tmpBowl.Price == 0 {
			return api.ErrorWrongOrderData
		}
		if tmpBowl.CustomFlag || tmpBowl.Modify {

			if tmpBowl.Items == nil {
				return api.ErrorWrongOrderData
			}
			if len(tmpBowl.Items) == 0 {
				return api.ErrorWrongOrderData
			}

			for _, i := range tmpBowl.Items {
				if i.Count == 0 {
					return api.ErrorWrongOrderData
				}
			}
		}
	}
	return 0
}

func (fm *FoodManager) VerifyComChangeModuleData(data *apiC.ComChangeModuleData) int {

	if data.NewModule > NumModules || data.NewModule < 1 {
		fmt.Println("New mod", data.NewModule)
		return api.ErrorWrongChangeModuleData
	}
	if data.OldModule > NumModules || data.OldModule < 1 {
		fmt.Println("Old mod", data.OldModule)
		return api.ErrorWrongChangeModuleData
	}
	if data.OldPos > fm.HardM.Modules[data.OldModule].Bowls.Len() || data.OldPos < 1 {
		fmt.Println("Old pos", data.OldPos)
		return api.ErrorWrongChangeModuleData
	}
	if data.NewPos < 1 {
		fmt.Println("New pos", data.NewPos)
		return api.ErrorWrongChangeModuleData
	}
	if !fm.HardM.Modules[data.NewModule].Enable {
		return api.ErrorWrongChangeModuleData
	}
	return 0
}

func (fm *FoodManager) VerifyComSetModuleStateData(data *apiC.ComSetModuleStateData) int {
	if data.Module < 1 || data.Module > NumModules {
		return api.ErrorWrongModuleData
	}
	return 0
}
