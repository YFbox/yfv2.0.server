package somelogger

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var LogDir string

type Slogger struct {
	sugar *zap.SugaredLogger
}

func InitNFSlogger() *Slogger {
	var logger, _ = zap.Config{
		Encoding:    "json",
		Level:       zap.NewAtomicLevelAt(zapcore.InfoLevel),
		OutputPaths: []string{"stdout"},
	}.Build()

	sl := logger.Sugar()
	return &Slogger{sugar: sl}
}
func InitFileSlogger(fileName string) *Slogger {
	dir := LogDir + "/server/log/"
	ok := createDirectory(dir)
	if !ok {
		log.Fatal("can not create directory " + dir)
	}
	var logger, _ = zap.Config{
		Encoding:    "json",
		Level:       zap.NewAtomicLevelAt(zapcore.InfoLevel),
		OutputPaths: []string{dir + fileName + ".log"},
	}.Build()

	sl := logger.Sugar()
	return &Slogger{sugar: sl}
}
func InitSlogger(fileName string) *Slogger {
	dir := LogDir + "/server/log/"

	ok := createDirectory(dir)
	if !ok {
		log.Fatal("can not create directory " + dir)
	}
	var logger, _ = zap.Config{
		Encoding:    "json",
		Level:       zap.NewAtomicLevelAt(zapcore.InfoLevel),
		OutputPaths: []string{"stdout", dir + fileName + ".log"},
	}.Build()

	sl := logger.Sugar()
	return &Slogger{sugar: sl}
}

func (sl *Slogger) LogFatal(err error) {
	sl.sugar.Panicw("",
		"panic", funcName(),
		err.Error(), "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
}

func (sl *Slogger) LogError(err error) {
	sl.sugar.Errorw("",
		"error", funcName(),
		err.Error(), "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
}

func (sl *Slogger) LogStrError(errCode int, desc string, userIp string) {
	sl.sugar.Errorw("",
		"source", funcName(),
		"error", errCode,
		"description", desc,
		"user", userIp,
		"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
}

func (sl *Slogger) LogInfo(info string) {
	sl.sugar.Infow("",
		"info", funcName(),
		info, "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
}

func funcName() string {
	pc, _, _, _ := runtime.Caller(2)
	path := strings.Split(runtime.FuncForPC(pc).Name(), ".")
	return path[1]
}

func createDirectory(dirName string) bool {
	src, err := os.Stat(dirName)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dirName, 0755)
		if errDir != nil {
			panic(err)
		}
		return true
	} else {
		return true
	}

	if src.Mode().IsRegular() {
		fmt.Println(dirName, "already exist as a file!")
		return false
	}

	return false
}
