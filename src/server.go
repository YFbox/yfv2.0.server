package main

/*
wi-fi:
"yf fm":
	password: "YOURFOODFM"
*/
import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os/user"

	"server/fillservice"
	"server/managerservice"
	"server/somelogger"

	"github.com/gorilla/websocket"
)

var sugar *somelogger.Slogger
var fm *managerservice.FoodManager

func main() {
	fPtr := flag.Bool("f", false, "fill database")
	fOnlyPtr := flag.Bool("fo", false, "only fill database")
	portPtr := flag.Int("p", 8080, "connection port")
	logDirPtr := flag.String("l", "", "log directory")
	flag.Parse()

	port := ""
	if portPtr != nil {
		if *portPtr > 1024 {
			port = fmt.Sprintf("%d", *portPtr)
		} else {
			port = "8080"
		}
	}

	if logDirPtr != nil {
		if *logDirPtr != "" {
			somelogger.LogDir = *logDirPtr
		} else {
			usr, err := user.Current()
			if err != nil {
				panic(err)
			}
			somelogger.LogDir = usr.HomeDir

		}
	}
	fmt.Println("logdir: ", somelogger.LogDir)

	sugar = somelogger.InitSlogger("serverMain")
	siteMux := http.NewServeMux()
	siteMux.HandleFunc("/yf", startHandler)

	sugar.LogInfo("Starting server at :" + port)

	fm = managerservice.NewManager()
	if fm == nil {
		sugar.LogError(errors.New("Can't init server manager!"))
		return
	}

	if *fPtr || *fOnlyPtr {
		fillservice.InitDatabase()
		if *fOnlyPtr {
			return
		}
	}
	go fm.Start()

	if err := http.ListenAndServe(":"+port, siteMux); err != nil {
		log.Fatalf("cannot listen: %s", err)
	}
}

func startHandler(w http.ResponseWriter, r *http.Request) {
	sugar.LogInfo(fmt.Sprintf("http request url(%s) ip(%s)", r.URL, r.RemoteAddr))
	upgrader := websocket.Upgrader{}
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	u := managerservice.GetUser(conn, r.RemoteAddr)
	fm.User <- u
}
