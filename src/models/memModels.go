package models

import "time"

type MemItem struct {
	Code       int
	Tray       string
	ThrowSpeed int
	Status     string
}

type MemBowl struct {
	Id       int
	Code     int
	Name     string
	ModuleId int

	QueuePos int
	OrderPos int

	Items  []*MemItem
	Sent   bool
	Loaded int
	Status string

	Order       *MemOrder
	ReadyTime   time.Time
	CookingTime int
}

type MemOrder struct {
	Id         int
	DayId      int
	PartnerId  int
	Bowls      []*MemBowl
	OrderTime  time.Time
	StartTime  time.Time
	ReadyTime  time.Time
	ClosedTime time.Time
	Status     string
}

var (
	LightModePowerOn   = 0
	LightModeWaiting   = 1
	LightModeCooking   = 2
	LightModeReady     = 3
	LightModeNeedPlate = 4
)

var (
	LcdModeWaiting   = 0
	LcdModeCooking   = 1
	LcdModeReady     = 2
	LcdModeNeedPlate = 3
)
