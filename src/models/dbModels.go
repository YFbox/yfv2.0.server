package models

import "time"

type DBService interface {
	InitService() error
}

const (
	OrderStatusPending   = "pending"
	OrderStatusLoading   = "loading"
	OrderStatusLoaded    = "loaded"
	OrderStatusCooking   = "cooking"
	OrderStatusReady     = "ready"
	OrderStatusClosed    = "closed"
	OrderStatusCancelled = "cancelled"
	OrderStatusErrClosed = "errclosed"
)

const (
	ItemTypeBase     = "base"
	ItemTypeProtein  = "protein"
	ItemTypeVeggies  = "veggies"
	ItemTypeSauce    = "sauce"
	ItemTypeUncknown = "uncknown"
)

const (
	BowlTypeSoup     = "soup"
	BowlTypeSalad    = "salad"
	BowlTypeUncknown = "uncknown"
)

const (
	BowlCategoryMenu     = "menu"
	BowlCategoryCustom   = "custom"
	BowlCategoryUncknown = "uncknown"
)

func StringInSlice(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}

// DbItem ..
type DbItem struct {
	Code        int     `json:"code"`
	Title       string  `json:"title"`
	Type        string  `json:"type"`
	Description string  `json:"description"`
	StorageTime int     `json:"storageTime"`
	Allergy     string  `json:"allergy"`
	Price       float32 `json:"price"`
	Picture     []byte  `json:"picture"`
	ThrowSpeed  int     `json:"throwSpeed"`
}

type DbAmountItem struct {
	ItemCode int `json:"itemCode"`
	Amount   int `json:"amount"`
	Reserved int `json:"reserved"`
	Expired  int `json:"expired"`
}

type DbCartridge struct {
	Id       int         `json:"id"`
	ItemCode int         `json:"itemCode"`
	LoadTime interface{} `json:"loadTime"`
}

type DbTray struct {
	ID      string    `json:"id"`
	Active  bool      `json:"active"`
	Batches []DbBatch `json: "batches"`
}

type DbBatch struct {
	ID         int           `json:"id"`
	LoadTime   interface{}   `json:"loadTime"`
	Cartridges []DbCartridge `json: cartridges`
}

type DbBowl struct {
	Code        int            `json:"code"`
	Name        string         `json:"name"`
	Type        string         `json:"type"`
	Items       []DbRecipeItem `json:"items"`
	Price       float32        `json:"price"`
	CookingTime int            `json:"cookingTime"`
	ShowInMenu  bool           `json:"showInMenu"`
	CustomFlag  bool           `json: customFlag`
	Modify      bool           `json: modify`
	Picture     []byte         `json:"picture"`
}

type DbRecipeItem struct {
	ItemCode   int `json:"itemCode"`
	ThrowSpeed int `json:"throwSpeed"`
	Count      int `json:"count"`
}

type DbAmountBowl struct {
	BowlCode int `json:"bowlCode"`
	Amount   int `json:"amount"`
}

type DbOrder struct {
	Id         int            `json:"id"`
	DayId      int            `json:"dayid"`
	PartnerId  int            `json:"partnerId"`
	Bowls      []DbBowlStruct `json: "bowls"`
	Total      float32        `json:"total"`
	OrderTime  time.Time      `json:"ordertime"`
	StartTime  time.Time      `json:"starttime"`
	ReadyTime  time.Time      `json:"readytime"`
	ClosedTime time.Time      `json:"closedtime"`
	Status     string         `json:"status"`
}
type DbBowlStruct struct {
	Code     int
	StructId int
}

type DbVerboseOrder struct {
	Id        int                     `json:"id"`
	OrderTime time.Time               `json:"ordertime"`
	Bowls     []*DbVerboseOrderStruct `json:"bowls"`
}

type DbVerboseCartridge struct {
	Code     int     `json: "code"`
	ItemCode int     `json: "itemCode"`
	Tray     string  `json: "tray"`
	Price    float32 `json: "price"`
}

type DbVerboseOrderStruct struct {
	StructId   int                  `json:"structId"`
	Bowl       *DbBowl              `json:"bowl"`
	ModuleId   int                  `json:"moduleId"`
	Cartridges []DbVerboseCartridge `json:"cartridges"`
	StartTime  time.Time            `json:"startTime"`
	ReadyTime  time.Time            `json:"readyTime"`
	CloseTime  time.Time            `json:"closeTime"`
}
