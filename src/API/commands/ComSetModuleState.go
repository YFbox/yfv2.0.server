package apiC

type ComSetModuleStateData struct {
	Module int  `json:"module"`
	State  bool `json:"state"` //values(on/off)
}
