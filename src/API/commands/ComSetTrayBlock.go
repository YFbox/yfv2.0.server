package apiC

type ComSetTrayBlockData struct {
	TrayId   string `json:"trayId"`
	IsActive bool   `json: "isActive"`
}
