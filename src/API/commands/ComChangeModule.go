package apiC

type ComChangeModuleData struct {
	Order     int `json:"order"`
	OldModule int `json:"old_module"`
	NewModule int `json:"new_module"`
	OldPos    int `json:"old_pos"`
	NewPos    int `json:"new_pos"`
}
