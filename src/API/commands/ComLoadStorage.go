package apiC

import "time"

type ComLoadStorageData struct {
	TrayId         string    `json: "trayId"`
	ItemCode       int       `json: "itemCode"`
	Amount         int       `json: "amount"`
	ProductionTime time.Time `json: "productionDateTime"`
}
