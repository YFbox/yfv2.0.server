package apiC

import "server/models"

type ComNewOrderData struct {
	PartnerOrderId int        `json: "partnerOrderId"`
	Bowls          []CNODBowl `json: "bowls"`
	Total          float32    `json:"total"`
}
type CNODBowl struct {
	Code       int                   `json:"code"`
	Name       string                `json:"name"`
	Items      []models.DbRecipeItem `json:"items"`
	Price      float32               `json:"price"`
	Type       string                `json:"type"`
	Modify     bool                  `json:"modify"`
	CustomFlag bool                  `json:"customFlag"`
	ShowInMenu bool                  `json:"showInMenu"`
	Count      int                   `json:"count"`
}
