package apiC

type ComOrderChangedData struct {
	Bowls []CNODBowl `json: ""bowls""`
	Total float32    `json:""total""`
}
