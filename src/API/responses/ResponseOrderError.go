package apiR

type ResponseOrderErrorData struct {
	OrderId   int `json: "orderId"`
	ErrorCode int `json: "errorCode"`
}
