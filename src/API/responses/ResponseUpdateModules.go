package apiR

type ResponseUpdateModulesData struct {
	Modules []ResponseUpdateModuleData `json: "modules"`
}
