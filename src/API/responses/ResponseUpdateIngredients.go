package apiR

import "server/models"

type ResponseUpdateIngredientsData struct {
	Items []models.DbItem `json:"items"`
}
