package apiR

import (
	"time"
	"server/models"
)

type ResponseUpdateHistoryData struct {
	Orders []OrderHistory `json: "Orders"`
}

type OrderHistory struct {
	OrderId int                 `json: "orderId"`
	Bowls   []BowlsVerboseState `json: "bowls"`
	DTime   time.Time           `json: "entryTime"`
}

type BowlsVerboseState struct {
	Code       int       						`json: "code"`
	Name       string    						`json: "name"`
	Price 	   float32  						`json: "price"`
	BType      string   	 					`json: "type"`     //uncknown,salad,soup..
	Category   string    						`json: "category"` //uncknown,custom,menu
	Cartridges []models.DbVerboseCartridge     	`json: "cartridges"`
	ModuleId   int       						`json: "module"`
	StartTime  time.Time 						`json: "startTime"`
	ReadyTime  time.Time 						`json: "readyTime"`
	ClosedTime time.Time 						`json: "closedTime"`
}
