package apiR

import "server/models"

type ResponseUpdateStorageData struct {
	Trays []models.DbTray `json: "trays"`
}
