package apiR

import "time"

type ResponseUpdateLcdData struct {
	ModuleId    int       `json: "moduleId"`
	Name        string    `json: "name"`
	OrderId     int       `json: "orderId"`
	BowlName    string    `json: "bowlName"`
	TimeCooking int       `json: "timecooking"`
	Mode        int       `json: "mode"`
	OrderTime   time.Time `json: "orderTime"`
}
