package apiR

import "server/models"

type ResponseUpdateBowlsData struct {
	Bowls []models.DbBowl `json: "bowls"`
}
