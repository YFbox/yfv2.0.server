package apiR

type ResponseUpdateModuleData struct {
	Id        int  `json: "id"`
	NeedPlate bool `json: "needPlate"`
	Enable    bool `json: "enable"`
	IsActive  bool `json: "isActive"`
	DishReady bool `json: "dishReady"`
}
