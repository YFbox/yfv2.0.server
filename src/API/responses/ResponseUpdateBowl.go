package apiR

import "time"

type ResponseUpdateBowlData struct {
	OrderId     int       `json: "order_id"`
	Code        int       `json: "code"`
	ModuleId    int       `json: "module"`
	QueuePos    int       `json: "queue_pos"`
	OrderPos    int       `json: "order_pos"`
	ItemsStatus []string  `json: "itemsStatus"`
	ReadyTime   time.Time `json: "readyTime"`
	Status      string    `json: "status"`
}
