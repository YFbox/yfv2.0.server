package apiR

import "server/models"

type ResponseUpdateTrayData struct {
	Tray models.DbTray `json: "tray"`
}
