package apiR

import "time"

type ResponseUpdateOrdersData struct {
	Orders []ROUDOrder `json: "orders"`
}

type ROUDOrder struct {
	Id        int        `json: "id"`
	Bowls     []ROUDBowl `json: "bowls"`
	Status    string     `json: "status"`
	OrderTime time.Time  `json:"orderTime"`
}
type ROUDBowl struct {
	Name        string     `json: "name"`
	Code        int        `json: "code"`
	Items       []ROUDItem `json: "items"`
	Status      string     `json: "status"`
	ModuleId    int        `json: "moduleid"`
	QueuePos    int        `json: "queue_pos"`
	CookingTime int        `json:"cookingTime"`
	ReadyTime   time.Time  `json: "readyTime"`
}

type ROUDItem struct {
	Code   int    `json: "code"`
	Status string `json: "status"`
}
