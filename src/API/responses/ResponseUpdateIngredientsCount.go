package apiR

import "server/models"

type ResponseUpdateIngredientsCountData struct {
	Items []models.DbAmountItem `json: "items"`
}
