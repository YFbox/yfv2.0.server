package apiR

import "server/models"

type ResponseUpdateBowlsCountData struct {
	Bowls []models.DbAmountBowl `json: "bowls"`
}
