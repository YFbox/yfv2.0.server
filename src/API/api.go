package api

import "errors"

const GLOBAL_PARTNER_KEY = "v3JQ0ypws4Q5XVO1kL30RuKkivGpz4FV"

//Incoming commands
const (
	ComNewOrder        string = "NewOrder"        //responses:"UpdateOrders","UpdateOrderNumber","UpdateBowls","UpdateBowlsCount","UpdateIngredientsCount"
	ComCancelOrder     string = "CancelOrder"     //responses:"UpdateOrders"
	ComGetBowls        string = "GetBowls"        //responses:"UpdateBowls","UpdateBowlsCount"
	ComLoadStorage     string = "LoadStorage"     //responses:"UpdateTray"
	ComGetIngredients  string = "GetIngredients"  //responses:"UpdateIngredients","UpdateIngredientsCount"
	ComGetStorage      string = "GetStorage"      //responses:"UpdateStorage"
	ComClearTray       string = "ClearTray"       //responses:"UpdateTray","UpdateIngredientsCount","UpdateBowlsCount"
	ComUnloadTray      string = "UnloadTray"      //responses:"UpdateTray","UpdateIngredientsCount","UpdateBowlsCount"
	ComTotalUnload     string = "TotalUnload"     //responses:"UpdateTray","UpdateIngredientsCount","UpdateBowlsCount"
	ComChangeModule    string = "ChangeModule"    //responses:"UpdateOrders"
	ComGetActiveOrders string = "GetActiveOrders" //responses:"UpdateOrders"
	ComGetModules      string = "GetModules"      //responses:"UpdateModules"
	ComSetModuleState  string = "SetModuleState"  //responses:"UpdateModule"
	ComGetHistory      string = "GetHistory"      //responses:"UpdateHistory"
	//ComOrderChanged    string = "OrderChanged"    //responses:"UpdateBowlsCount","UpdateIngredientsCount"
	ComSetTrayBlock   string = "SetTrayBlock"   //responses: "UpdateTray"
	ComDeleteFromMenu string = "DeleteFromMenu" //responses: "UpdateBowls","UpdateBowlsCount"

	ComInitModuleLcd   string = "InitModuleLcd"   //responses: "UpdateLcd"
	ComChangeModuleLcd string = "ChangeModuleLcd" //responses: "UpdateLcd"
)

//Outgoing responses
const (
	ResponseUpdateOrders           string = "UpdateOrders"
	ResponseUpdateOrderNumber      string = "UpdateOrderNumber"
	ResponseUpdateBowls            string = "UpdateBowls"
	ResponseUpdateBowl             string = "UpdateBowl"
	ResponseUpdateBowlsCount       string = "UpdateBowlsCount"
	ResponseUpdateStorage          string = "UpdateStorage"
	ResponseUpdateTray             string = "UpdateTray"
	ResponseUpdateModules          string = "UpdateModules"
	ResponseUpdateModule           string = "UpdateModule"
	ResponseUpdateIngredients      string = "UpdateIngredients"
	ResponseUpdateIngredientsCount string = "UpdateIngredientsCount"
	ResponseUpdateHistory          string = "UpdateHistory"
	ResponseUpdateLcd              string = "UpdateLcd"
	ResponseOrderError             string = "OrderError"
	ResponseError                  string = "ResponseError"
)

//System errors
var (
	ErrorInternal int = 1

	//order errors
	ErrorWrongOrderData        int = 101
	ErrorNoModulesConnected    int = 102
	ErrorUnknownRecipe         int = 103
	ErrorNotEnoughItems        int = 104
	ErrorUnknownBowl           int = 105
	ErrorWrongChangeModuleData int = 106
	ErrorDueUnloading          int = 107

	//storage errors
	ErrorWrongTrayData          int = 201
	ErrorCanNotGetBowl          int = 202
	ErrorCanNotGetHistory       int = 203
	ErrorCanNotGetAvailableBowl int = 204
	ErrorCanNotGetAvailableItem int = 205
	ErrorCanNotGetTray          int = 206
	ErrorCanNotGetItem          int = 207
	ErrorCanNotReserveItem      int = 208
	ErrorCanNotDeleteOrder      int = 209
	ErrorCanNotSetOrderStatus   int = 210
	ErrorCanNotSetBowlStatus    int = 211
	ErrorCanNotGetStorage       int = 212

	//modules errors
	ErrorWrongModuleData int = 301
	ErrorTooMuchLcd      int = 302

	//QueueFunc errors
	ErrorWrongRemoveBowl  error = errors.New("WrongRemoveBowl")
	ErrorWrongRemoveOrder error = errors.New("WrongRemoveOrder")
	ErrorWrongInsertBowl  error = errors.New("WrongInsertBowl")
)
