package canservice

import (
	"container/list"
	"sync"

	"server/somelogger"
)

var cmLogger *somelogger.Slogger

type CanManager struct {
	SerialConn SerialConn
	ConnErr    error
	Port       Canport
	TxMsg      TCanMsg
	RxMsg      chan *RCanMsg
	Mutex      *sync.Mutex
	CloseFlag  bool
	IsFree     bool

	SendQueue        *list.List
	RxMsgMap         map[uint32]uint8
	TxMsgMap         map[uint32]uint8
	Sending_flag     bool
	Is_confirmed     bool
	Check_target     bool
	Sending_attempts uint16
	Sending_timeout  int
	Source_id        uint8
	Target_id        uint8
	Error_flag       bool
}
type Canport struct {
	channel string
	baud    string
}

type TCanMsg struct {
	Id   string
	Dlc  string
	Data string
}
type CTCanMsg struct {
	msg       TCanMsg
	target_id uint8
}
type RCanMsg struct {
	Id   uint32
	Dlc  uint8
	Data [8]uint8
}
