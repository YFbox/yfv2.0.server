package canproto

const (

	/*
		MANIPULATOR MSG ADDR
	*/

	//**********COMMAND ADDR*************

	//1 byte data
	//#0 - reserved by CAN
	MAN_COM_NET_INIT = BASE_CAN_ADDR_MAN_STM + 0x001

	//2 byte data
	//#0 - reserved by CAN
	//#1 - ON/OFF
	MAN_COM_TEST_MODE = BASE_CAN_ADDR_MAN_STM + 0x002

	//6 bytes data
	//#0 - reserved by CAN
	//#1 - module number
	//#2#3 - tray
	//#4 - amount
	//#5 - priority
	MAN_COM_PROCESS_ITEM = BASE_CAN_ADDR_MAN_STM + 0x003

	//1 byte data
	//#0 - reserved by CAN
	MAN_COM_GET_STATE = BASE_CAN_ADDR_MAN_STM + 0x004

	//4 bytes data
	//#0 - reserved by CAN
	//#1#2 - tray
	//#3 - amount
	MAN_COM_UNLOAD_TRAY = BASE_CAN_ADDR_MAN_STM + 0x00C

	//7 bytes data
	//#0 - reserved by CAN
	//#1 - tray row
	//#2 - tray col
	//#3#4 - X pos
	//#5#6 - Y pos
	MAN_COM_SET_TRAY = BASE_CAN_ADDR_MAN_STM + 0x00F

	//**********TEST COMMANDS ADDR*********
	//3 bytes data
	//#0 - reserved by CAN
	//#0#1 - ID
	MAN_COM_GET_TO_ITEM = BASE_CAN_ADDR_MAN_STM + 0x005

	//5 bytes data
	//#0 - reserved by CAN
	//#0#1 - Xpos
	//#2#3 - Ypos
	MAN_COM_GET_TO_POINT = BASE_CAN_ADDR_MAN_STM + 0x006

	//1 byte data
	//#0 - reserved by CAN
	MAN_COM_UNLOAD = BASE_CAN_ADDR_MAN_STM + 0x007

	//2 byte
	//#0 - reserved by CAN
	//#0 - (1-on)/(0-off)
	MAN_COM_ACTUATOR = BASE_CAN_ADDR_MAN_STM + 0x008

	//***************REPLY ADDR************

	//1 byte data
	//#0 - reserved by CAN
	MAN_REPLY_CONNECTED = BASE_CAN_ADDR_MAN_STM + 0x009

	//2 byte data
	//#0 - reserved by CAN
	//#1 - man state
	MAN_REPLY_STATE = BASE_CAN_ADDR_MAN_STM + 0x00A

	//5 byte data
	//#0 - reserved by CAN
	//#1 - module number
	//#2#3 - tray
	//#4 - amount
	MAN_REPLY_STATE_LOADING = BASE_CAN_ADDR_MAN_STM + 0x00B

	//3 byte data
	//#0 - reserved by CAN
	//#1#2 - tray
	MAN_REPLY_START_TRAY_UNLOADING = BASE_CAN_ADDR_MAN_STM + 0x00D

	//3 byte data
	//#0 - reserved by CAN
	//#1#2 - tray
	MAN_REPLY_TRAY_UNLOADED = BASE_CAN_ADDR_MAN_STM + 0x00E
)
