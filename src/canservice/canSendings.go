package canservice

import (
	canproto "server/canservice/canProtoConfig"
	"server/models"
)

func (man *CanManager) SendBowl(b *models.MemBowl) {
	bId0 := byte(b.Id)
	bId1 := byte(b.Id >> 8)
	device_id := (b.ModuleId + canproto.CAN_ID_UNPACKER_M0)
	man.CanSend(uint32((device_id<<canproto.CAN_DEVICE_ADDR_OFFSET)+canproto.UNPACKER_COM_ADD_BOWL), []uint8{bId0, bId1, uint8(len(b.Items))}, 3, true, uint8(device_id))

	for i := 0; i < len(b.Items); {
		amount := uint8(1)
		for id := i + 1; id < len(b.Items); id++ {
			if b.Items[id].Tray == b.Items[i].Tray {
				amount++
			}
		}
		man.CanSend(uint32((device_id<<canproto.CAN_DEVICE_ADDR_OFFSET)+canproto.UNPACKER_COM_ADD_ITEM), []uint8{bId0, bId1, uint8(StrHexToInt(b.Items[i].Tray[2:4])), uint8(StrHexToInt(b.Items[i].Tray[0:2])), amount, uint8(b.Items[i].ThrowSpeed)}, 6, true, uint8(device_id))
		i += int(amount)
	}
}

func (man *CanManager) SendUnload(tray string, count int) {
	man.CanSend(uint32(canproto.MAN_COM_UNLOAD_TRAY), []uint8{uint8(StrHexToInt(tray[2:4])), uint8(StrHexToInt(tray[0:2])), uint8(count)}, 3, true, uint8(canproto.CAN_ID_MAN_STM))
}

func (man *CanManager) SendBowlCancelled(b *models.MemBowl) {
	// bId := []byte{byte(b.Id), byte(b.Id >> 8)}
	// man.SendQueue.PushBack(TCanMsg{
	// 	Id:   fmt.Sprintf("%03x", StackComCancelBOWL),
	// 	Dlc:  fmt.Sprintf("%01x", 3),
	// 	Data: fmt.Sprintf("%02x%02x%02x", b.ModuleId, bId[0], bId[1])})

}

func (man *CanManager) SendInitModule(module int) {
	device_id := module + canproto.CAN_ID_UNPACKER_M0
	base_addr := (device_id << canproto.CAN_DEVICE_ADDR_OFFSET)
	man.CanSend(uint32(base_addr+canproto.UNPACKER_COM_NET_INIT), []uint8{}, 0, false, uint8(device_id))
}

func (man *CanManager) SendManThrow(tray string, count int) {
	// man.SendQueue.PushBack(TCanMsg{
	// 	Id:   fmt.Sprintf("%03x", ManComItemsToThrow),
	// 	Dlc:  fmt.Sprintf("%01x", 3),
	// 	Data: fmt.Sprintf("%s%s%02x", tray[2:4], tray[0:2], count)})
}

func (man *CanManager) SendConvPump(on uint8) {
	man.CanSend(uint32(canproto.CONV_PUMP), []uint8{on}, 1, true, uint8(canproto.CAN_ID_CONV_STM))
}

func (man *CanManager) SetLightMode(module_id int, mode uint8) {
	device_id := module_id + canproto.CAN_ID_LIGHT_M0
	man.CanSend(uint32((device_id<<canproto.CAN_DEVICE_ADDR_OFFSET)+canproto.LIGHT_COM_SET_MODE), []uint8{mode}, 1, false, uint8(device_id))
}

func (man *CanManager) SetTrayPos(tray string, posX uint16, posY uint16) {
	man.CanSend(uint32(canproto.MAN_COM_SET_TRAY), []uint8{uint8(StrHexToInt(tray[2:4])), uint8(StrHexToInt(tray[0:2])), uint8(posX), uint8(posX >> 8), uint8(posY), uint8(posY >> 8)}, 6, true, uint8(canproto.CAN_ID_MAN_STM))
}
