package canservice

import (
	"time"

	"github.com/tarm/serial"
)

// var logger, _ = zap.Config{
// 	Encoding:    "json",
// 	Level:       zap.NewAtomicLevelAt(zapcore.InfoLevel),
// 	OutputPaths: []string{"stdout", "../log/serial.log"},
// }.Build()

// var sugar = logger.Sugar()

type SerialConn struct {
	Port      *serial.Port
	Buf       string
	CloseFlag bool
}

func (conn *SerialConn) Open(name string, baud int) error {
	var err error
	c := &serial.Config{Name: name, Baud: baud}
	conn.Port, err = serial.OpenPort(c)
	if err != nil {
		// sugar.Errorw("Serial failed connect to the port"+name,
		// 	"error", "SERIAL",
		// 	err, "time "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return err
	}
	// sugar.Infow("",
	// 	"info", "SERIAL",
	// 	"Serial connected to", name)
	return nil

}

func (conn *SerialConn) Write(data []byte) {
	_, err := conn.Port.Write(data)
	if err != nil {
		// sugar.Errorw("Serial can't write data",
		// 	"error", err,
		// 	"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
}

func (conn *SerialConn) Read() {
	for !conn.CloseFlag {
		lbuf := make([]byte, 27)
		n, err := conn.Port.Read(lbuf)
		if n > 0 && err == nil {
			conn.Buf += string(lbuf)
		}
		time.Sleep(100 * time.Microsecond)
	}
}
