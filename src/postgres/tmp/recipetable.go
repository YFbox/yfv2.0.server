package postgres

import (
	"fmt"
	"strings"
)

func (s *PostgresService) AddRecipe(BowlName string, Items []string) error {
	rows := []string{}
	for pos, item := range Items {
		row := fmt.Sprintf("(%d,'%s','%s')", pos, BowlName, item)
		rows = append(rows, row)
	}
	query := "INSERT INTO recipe(position,bowl_name,item_title) VALUES " + strings.Join(rows, ",")
	_, err := s.db.Exec(query)

	if err != nil {
		return err
	}
	return nil
}
func (s *PostgresService) DeleteRecipe(Name string) error {
	query := "DELETE FROM recipe WHERE bowl_name = $1"
	_, err := s.db.Exec(query, Name)

	if err != nil {
		return err
	}
	return nil
}
func (s *PostgresService) GetRecipe(BowlName string) ([]string, error) {
	recipe := []string{}

	query := "select item_title from recipe  where bowl_name = $1 order by position ASC"
	rows, err := s.db.Query(query, BowlName)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var title string

		err := rows.Scan(&title)

		if err != nil {
			return nil, err
		}
		recipe = append(recipe, title)
	}
	return recipe, nil
}
