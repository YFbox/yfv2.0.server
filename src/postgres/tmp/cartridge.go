package postgres

import "time"

func (s *PostgresService) StartDaySession() (string, error) {
	id := ""
	query := "select id from daysession where date(daytime)=$1"
	row := s.db.QueryRow(query, time.Now().Format("2006-01-02"))
	err := row.Scan(&id)
	if err != nil {
		query := "INSERT INTO daysession (daytime) VALUES ($1) returning id"
		row = s.db.QueryRow(query, time.Now().Format("2006-01-02 15:04:05"))
		err = row.Scan(&id)
	}

	return id, err
}
