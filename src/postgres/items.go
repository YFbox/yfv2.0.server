package postgres

import (
	"errors"
	api "server/API"
	"server/models"
)

func (s *PostgresService) InsertItem(i models.DbItem) (int, error) {
	var code int
	query := "INSERT INTO item(title, item_type,item_description,storage_time,allergy,price,picture,throw_speed) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) returning code"
	err := s.db.QueryRow(query, i.Title, i.Type, i.Description, i.StorageTime, i.Allergy, i.Price, i.Picture, i.ThrowSpeed).Scan(&code)
	if err != nil {
		pgLogger.LogError(err)
		return 0, errors.New("Can't insert item")
	}
	return code, nil
}

func (s *PostgresService) DeleteItem(code string) error {
	query := "DELETE FROM item WHERE code = $1"
	_, err := s.db.Exec(query, code)
	if err != nil {
		pgLogger.LogError(err)
		return errors.New("Can't delete item")
	}
	return nil
}

func (s *PostgresService) GetItems() ([]models.DbItem, int) {
	itemArr := []models.DbItem{}

	query := "SELECT * FROM item order by code"
	rows, err := s.db.Query(query)
	if err != nil {
		pgLogger.LogFatal(err)
	}

	for rows.Next() {
		item := models.DbItem{}

		err := rows.Scan(&item.Code, &item.Title, &item.Type, &item.ThrowSpeed, &item.Description, &item.StorageTime, &item.Allergy, &item.Price, &item.Picture)

		if err != nil {
			pgLogger.LogError(err)
			return nil, api.ErrorCanNotGetItem
		}

		itemArr = append(itemArr, item)
	}
	return itemArr, 0
}

func (s *PostgresService) GetAvailableItems() ([]models.DbAmountItem, int) {
	s.UpdateExpired()
	aItemArr := []models.DbAmountItem{}

	query := "SELECT * FROM amount_items order by item_code"
	rows, err := s.db.Query(query)
	if err != nil {
		pgLogger.LogError(err)
		return nil, api.ErrorCanNotGetAvailableItem
	}

	for rows.Next() {
		aitem := models.DbAmountItem{}

		err := rows.Scan(&aitem.ItemCode, &aitem.Amount, &aitem.Reserved, &aitem.Expired)

		if err != nil {
			pgLogger.LogError(err)
			return nil, api.ErrorCanNotGetAvailableItem
		}

		aItemArr = append(aItemArr, aitem)
	}
	return aItemArr, 0
}

func (s *PostgresService) GetAvailableItem(code int) (int, int) {
	s.UpdateExpired()
	var (
		amount   int
		reserved int
		expired  int
	)
	query := "SELECT amount,reserved,expired FROM amount_items where item_code =$1 order by item_code"
	err := s.db.QueryRow(query, code).Scan(&amount, &reserved, &expired)
	if err != nil {
		pgLogger.LogError(err)
		return 0, api.ErrorCanNotGetAvailableItem
	}
	return amount - reserved - expired, 0
}

func (s *PostgresService) ReserveItems(iCode int, count int) int {
	query := "update amount_items set reserved = reserved + $2 where item_code = $1"
	_, err := s.db.Exec(query, iCode, count)
	if err != nil {
		pgLogger.LogError(err)
		return api.ErrorCanNotReserveItem
	}
	return 0
}

func (s *PostgresService) PickUpItem(iCode int) (string, *models.DbCartridge, error) {
	var tId string
	var cId int
	tquery := "select tb.tray,c.id from tray_batch tb join batch_cartridge bc on bc.batch = tb.batch join cartridge c on c.id = bc.cartridge join tray t on t.id = tb.tray join item i on i.code = c.item_code where c.item_code = $1 and t.active = true and extract(DAY from now()-c.load_time)*24 + extract(HOUR from now()-c.load_time) <= i.storage_time group by tb.tray,c.id order by c.id,c.load_time"
	err := s.db.QueryRow(tquery, iCode).Scan(&tId, &cId)
	if err != nil {
		pgLogger.LogFatal(err)
		return "", nil, errors.New("Can't pickUp item")
	}

	cart := &models.DbCartridge{}
	cquery := "delete from cartridge where id = $1 returning *"
	err = s.db.QueryRow(cquery, cId).Scan(&cart.Id, &cart.ItemCode, &cart.LoadTime)
	if err != nil {
		pgLogger.LogFatal(err)
		return "", nil, errors.New("Can't pickUp item")
	}

	aiquery := "update amount_items set reserved = reserved - 1 where item_code = $1"
	_, err = s.db.Exec(aiquery, iCode)
	if err != nil {
		pgLogger.LogFatal(err)
		return "", nil, errors.New("Can't pickUp item")
	}
	return tId, cart, nil
}

func (s *PostgresService) GetItemByCode(iCode int) (models.DbItem, error) {
	var item models.DbItem
	query := "select code,title,item_type,throw_speed,item_description,storage_time,allergy,price from item where code =$1"
	err := s.db.QueryRow(query, iCode).Scan(&item.Code, &item.Title, &item.Type, &item.ThrowSpeed, &item.Description, &item.StorageTime, &item.Allergy, &item.Price)
	if err != nil {
		pgLogger.LogFatal(err)
		return item, errors.New("Can't pickUp item")
	}
	return item, nil
}
