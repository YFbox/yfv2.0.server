package postgres

import (
	"errors"
	"fmt"
	api "server/API"
	"server/models"
	"strings"
)

func (s *PostgresService) InsertBowl(b models.DbBowl) (int, error) {

	var code int
	query := "INSERT INTO bowl(name,bowl_type,price,cooking_time,custom_flag,is_menu_flag,modified,picture) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) returning code"
	err := s.db.QueryRow(query, b.Name, b.Type, b.Price, b.CookingTime, b.CustomFlag, b.ShowInMenu, b.Modify, b.Picture).Scan(&code)
	if err != nil {
		pgLogger.LogFatal(err)
		return 0, errors.New("Can't insert bowl")
	}
	substr := []string{}
	for pos, rI := range b.Items {
		substr = append(substr, fmt.Sprintf("(%d,%d,%d,%d)", code, pos, rI.ItemCode, rI.Count))
	}

	rquery := "insert into recipe (bowl_code,position,item_code,count) values " + strings.Join(substr, ",")
	_, err = s.db.Exec(rquery)
	if err != nil {
		pgLogger.LogFatal(err)
	}
	return code, nil
}

func (s *PostgresService) DeleteBowl(Code int) error {
	query := "DELETE FROM bowl WHERE code = $1"
	_, err := s.db.Exec(query, Code)
	return err
}
func (s *PostgresService) DeleteBowlFromMenu(Code int) error {

	query := "update bowl set is_menu_flag = false where code = $1"
	_, err := s.db.Exec(query, Code)
	if err != nil {
		pgLogger.LogFatal(err)
		return err
	}
	return nil
}
func (s *PostgresService) GetBowl(code int) (*models.DbBowl, int) {
	query := "select b.code,b.name,b.bowl_type,b.price,b.cooking_time,b.custom_flag,b.is_menu_flag,r.item_code,r.count,i.throw_speed from bowl b join recipe r on r.bowl_code = b.code join item i on i.code = r.item_code  where b.code = $1 group by b.code,bowl_code,r.item_code,r.count,r.position,i.throw_speed order by b.code, r.position"
	rows, err := s.db.Query(query, code)
	if err != nil {
		pgLogger.LogFatal(err)
		return nil, api.ErrorCanNotGetBowl
	}

	bowl := &models.DbBowl{}
	for rows.Next() {
		item := models.DbRecipeItem{}
		err := rows.Scan(&bowl.Code, &bowl.Name, &bowl.Type, &bowl.Price, &bowl.CookingTime, &bowl.CustomFlag, &bowl.ShowInMenu, &item.ItemCode, &item.Count, &item.ThrowSpeed)
		if err != nil {
			pgLogger.LogFatal(err)
			return nil, api.ErrorCanNotGetBowl
		}
		bowl.Items = append(bowl.Items, item)
	}
	if len(bowl.Items) == 0 {
		return nil, api.ErrorCanNotGetBowl
	}
	return bowl, 0
}

func (s *PostgresService) GetBowls() ([]models.DbBowl, int) {
	BowlArr := []models.DbBowl{}
	query := "select b.code,b.name,b.bowl_type,b.price,b.cooking_time,b.custom_flag,is_menu_flag,b.picture,r.item_code,r.count from bowl b join recipe r on r.bowl_code = b.code where b.is_menu_flag = true group by b.code,bowl_code,r.item_code,r.count,r.position order by b.code, r.position"
	rows, err := s.db.Query(query)
	if err != nil {
		pgLogger.LogFatal(err)
		return nil, api.ErrorCanNotGetBowl
	}
	var prevBowl *models.DbBowl
	for rows.Next() {
		bowl := models.DbBowl{}
		item := models.DbRecipeItem{}

		err := rows.Scan(&bowl.Code, &bowl.Name, &bowl.Type, &bowl.Price, &bowl.CookingTime, &bowl.CustomFlag, &bowl.ShowInMenu, &bowl.Picture, &item.ItemCode, &item.Count)

		if err != nil {
			pgLogger.LogFatal(err)
			return nil, api.ErrorCanNotGetBowl
		}
		if prevBowl != nil {
			if prevBowl.Code == bowl.Code {
				prevBowl.Items = append(prevBowl.Items, item)
			} else {
				BowlArr = append(BowlArr, *prevBowl)
				bowl.Items = append(bowl.Items, item)
				prevBowl = &bowl
			}
		} else {
			bowl.Items = append(bowl.Items, item)
			prevBowl = &bowl
		}

	}
	BowlArr = append(BowlArr, *prevBowl)
	return BowlArr, 0
}

func (s *PostgresService) GetAvailableBowls() ([]models.DbAmountBowl, int) {
	aBowlArr := []models.DbAmountBowl{}

	query := "SELECT ab.bowl_code,ab.amount FROM amount_bowls ab join bowl b on b.code =  ab.bowl_code where b.is_menu_flag = true order by bowl_code"
	rows, err := s.db.Query(query)
	if err != nil {
		pgLogger.LogFatal(err)
		return nil, api.ErrorCanNotGetAvailableBowl
	}

	for rows.Next() {
		aBowl := models.DbAmountBowl{}

		err := rows.Scan(&aBowl.BowlCode, &aBowl.Amount)

		if err != nil {
			pgLogger.LogFatal(err)
			return nil, api.ErrorCanNotGetAvailableBowl
		}

		aBowlArr = append(aBowlArr, aBowl)
	}
	return aBowlArr, 0
}

func (s *PostgresService) GetAvailableBowl(code int) (int, int) {
	var amount int
	query := "SELECT amount FROM amount_bowls where bowl_code = $1 order by bowl_code"
	err := s.db.QueryRow(query, code).Scan(&amount)
	if err != nil {
		pgLogger.LogFatal(err)
		return amount, api.ErrorCanNotGetAvailableBowl
	}
	return amount, 0
}
func (s *PostgresService) CheckBowlCode(code int) bool {
	q := "select code from bowl where code = $1"
	var id int
	err := s.db.QueryRow(q, code).Scan(&id)
	if err != nil {
		return false
	}
	if id != code {
		return false
	}
	return true
}
