package postgres

import (
	"database/sql"
	"fmt" // "io/ioutil"
	"io/ioutil"
	"server/somelogger"

	_ "github.com/lib/pq"
)

var Db = PostgresService{}
var pgLogger = somelogger.InitNFSlogger()

type PostgresService struct {
	db *sql.DB
}

func InitService() error {
	var err error
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME)
	Db.db, err = sql.Open("postgres", dbinfo)
	if err != nil {
		return err
	}

	if err = Db.db.Ping(); err != nil {
		return err
	}
	return nil
}

func (s *PostgresService) InitFromFile(name string) error {
	initDB, err := ioutil.ReadFile(name)
	if err != nil {
		return err
	}
	_, ierr := s.db.Exec(string(initDB))
	if ierr != nil {
		return ierr
	}
	return nil
}

func (s *PostgresService) InitFromBytes(file []byte) error {

	_, ierr := s.db.Exec(string(file))
	if ierr != nil {
		return ierr
	}
	return nil
}
