package postgres

import (
	"fmt"
	api "server/API"
	"server/models"
	"strings"
	"time"
)

func (s *PostgresService) InsertTray(Id string, Active bool) error {

	query := "INSERT INTO tray(id,active) VALUES ($1,$2)"
	_, err := s.db.Exec(query, Id, Active)
	return err
}

func (s *PostgresService) DeleteTray(Id string) error {
	query := "DELETE FROM tray WHERE id = $1"
	_, err := s.db.Exec(query, Id)
	return err
}

func (s *PostgresService) GetTray(Id string) (*models.DbTray, int) {
	tr := &models.DbTray{}

	query := "SELECT * FROM tray where id = $1"
	err := s.db.QueryRow(query, Id).Scan(&tr.ID, &tr.Active)
	if err != nil {
		pgLogger.LogError(err)
		return nil, api.ErrorCanNotGetTray
	}
	bq := "select b.id,b.load_time,c.id,c.item_code,c.load_time from tray_batch tb join batch b on tb.batch = b.id join batch_cartridge bc on bc.batch = b.id join cartridge c on c.id = bc.cartridge where tb.tray = $1 group by b.id,b.load_time,c.id,c.item_code,c.load_time order by b.load_time"
	brows, berr := s.db.Query(bq, tr.ID)

	if berr != nil {
		pgLogger.LogError(err)
		return nil, api.ErrorCanNotGetTray
	}
	var prev_batch *models.DbBatch
	for brows.Next() {
		b := models.DbBatch{}
		c := models.DbCartridge{}
		berr = brows.Scan(&b.ID, &b.LoadTime, &c.Id, &c.ItemCode, &c.LoadTime)
		if berr == nil {
			if prev_batch != nil {
				if prev_batch.ID == b.ID {
					prev_batch.Cartridges = append(prev_batch.Cartridges, c)
				} else {
					tr.Batches = append(tr.Batches, *prev_batch)
					b.Cartridges = append(b.Cartridges, c)
					prev_batch = &b
				}
			} else {
				b.Cartridges = append(b.Cartridges, c)
				prev_batch = &b
			}
		} else {
			pgLogger.LogFatal(berr)
		}
	}
	if prev_batch != nil {
		tr.Batches = append(tr.Batches, *prev_batch)
	}
	return tr, 0
}

func (s *PostgresService) GetTrays() ([]models.DbTray, int) {
	trayArr := []models.DbTray{}

	query := "SELECT * FROM tray order by id ASC"
	rows, err := s.db.Query(query)
	if err != nil {
		pgLogger.LogError(err)
		return nil, api.ErrorCanNotGetStorage
	}
	for rows.Next() {
		tr := models.DbTray{}
		err = rows.Scan(&tr.ID, &tr.Active)
		if err != nil {
			pgLogger.LogError(err)
			return nil, api.ErrorCanNotGetStorage
		}
		bq := "select b.id,b.load_time,c.id,c.item_code,c.load_time from tray_batch tb join batch b on tb.batch = b.id join batch_cartridge bc on bc.batch = b.id join cartridge c on c.id = bc.cartridge where tb.tray = $1 group by b.id,b.load_time,c.id,c.item_code,c.load_time order by b.load_time"
		brows, berr := s.db.Query(bq, tr.ID)
		if berr == nil {
			var prev_batch *models.DbBatch
			for brows.Next() {
				b := models.DbBatch{}
				c := models.DbCartridge{}
				berr = brows.Scan(&b.ID, &b.LoadTime, &c.Id, &c.ItemCode, &c.LoadTime)
				if berr == nil {
					if prev_batch != nil {
						if prev_batch.ID == b.ID {
							prev_batch.Cartridges = append(prev_batch.Cartridges, c)
						} else {
							tr.Batches = append(tr.Batches, *prev_batch)
							b.Cartridges = append(b.Cartridges, c)
							prev_batch = &b
						}
					} else {
						b.Cartridges = append(b.Cartridges, c)
						prev_batch = &b
					}
				} else {
					pgLogger.LogFatal(berr)
				}
			}
			if prev_batch != nil {
				tr.Batches = append(tr.Batches, *prev_batch)
			}
		}

		trayArr = append(trayArr, tr)
	}
	return trayArr, 0
}
func (s *PostgresService) ClearTray(Id string) int {
	tquery := "select * from tray where id = $1"
	_, err := s.db.Exec(tquery, Id)
	if err != nil {
		pgLogger.LogError(err)
		return api.ErrorWrongTrayData
	}

	query := "delete from batch where id in(select batch from tray_batch where tray = $1)"
	_, err = s.db.Exec(query, Id)
	if err != nil {
		pgLogger.LogError(err)
		return api.ErrorInternal
	}
	return 0
}

func (s *PostgresService) LoadTray(Id string, ItemCode int, Amount int, ProdTime time.Time) int {
	if Amount <= 0 {
		return api.ErrorWrongTrayData
	}
	var (
		tActive bool
	)
	tquery := "select active from tray where id = $1"
	err := s.db.QueryRow(tquery, Id).Scan(&tActive)
	if err != nil {
		pgLogger.LogError(err)
		return api.ErrorWrongTrayData
	}
	if tActive {
		var batchId int
		bquery := "insert into batch default values returning id"
		err = s.db.QueryRow(bquery).Scan(&batchId)
		if err != nil {
			pgLogger.LogError(err)
			return api.ErrorInternal
		}

		tbquery := fmt.Sprintf("insert into tray_batch (tray,batch) VALUES ($1,$2)")
		_, err = s.db.Exec(tbquery, Id, batchId)
		if err != nil {
			s.DeleteBatch(batchId)
			pgLogger.LogError(err)
			return api.ErrorInternal
		}

		subq := []string{}
		for i := 0; i < Amount; i++ {
			//subq = append(subq, fmt.Sprintf("(%d,'%s')", ItemCode, ProdTime.Format("2006-01-02 15:04:05")))
			subq = append(subq, fmt.Sprintf("(%d,now())", ItemCode))
		}

		iquery := "insert into cartridge (item_code,load_time) values " + strings.Join(subq, ",") + "returning id"
		rows, err := s.db.Query(iquery)
		if err != nil {
			pgLogger.LogError(err)
			s.DeleteBatch(batchId)
			return api.ErrorInternal
		}

		bcsubq := []string{}
		for rows.Next() {
			var cId int
			ierr := rows.Scan(&cId)
			if ierr == nil {
				bcsubq = append(bcsubq, fmt.Sprintf("(%d,%d)", batchId, cId))
			}
		}
		bcquery := "insert into batch_cartridge (batch,cartridge) values " + strings.Join(bcsubq, ",")
		_, err = s.db.Exec(bcquery)
		if err != nil {
			pgLogger.LogError(err)
			s.DeleteBatch(batchId)
			return api.ErrorInternal
		}
		return 0
	}
	return api.ErrorWrongTrayData
}

func (s *PostgresService) SetTrayState(Id string, State bool) int {
	if State == false {
		query := "select ai.reserved from tray_batch tb join batch_cartridge bc on bc.batch = tb.batch join cartridge c on c.id = bc.cartridge join tray t on t.id = tb.tray join amount_items ai on ai.item_code = c.item_code where t.id = $1 group by tb.tray,c.id,ai.reserved order by c.id,c.load_time"
		rows, _ := s.db.Query(query, Id)
		for rows.Next() {
			var reserved int
			err := rows.Scan(&reserved)
			if err != nil {
				pgLogger.LogError(err)
				return api.ErrorWrongTrayData
			}
			if reserved > 0 {
				return api.ErrorWrongTrayData
			} else {
				s.ClearTray(Id)

			}
		}

	}
	tquery := "update tray set active = $1 where id = $2"
	_, err := s.db.Exec(tquery, State, Id)
	if err != nil {
		pgLogger.LogError(err)
		return api.ErrorWrongTrayData
	}
	return 0
}

func (s *PostgresService) DeleteBatch(Id int) error {
	query := "DELETE FROM batch WHERE id = $1"
	_, err := s.db.Exec(query, Id)
	return err
}
