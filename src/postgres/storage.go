package postgres

import (
	"errors"
)

func (s *PostgresService) CheckOverdue() (map[string]int, error) {
	query := "select tray,count(id) from (select tb.tray,c.id from batch b join tray_batch tb on tb.batch = b.id join batch_cartridge bc on bc.batch = b.id join cartridge c on bc.cartridge = c.id join item i on i.code = c.item_code where extract(DAY from now()-b.load_time)*24 + extract(HOUR from now()-b.load_time) >= i.storage_time group by tb.tray,c.id,b.load_time order by b.load_time) as sl group by sl.tray"
	rows, err := s.db.Query(query)
	if err != nil {
		pgLogger.LogError(err)
		return nil, errors.New("Can not check storage")
	}
	arr := map[string]int{}
	for rows.Next() {
		var (
			tray  string
			count int
		)
		err = rows.Scan(&tray, &count)
		if err != nil {
			pgLogger.LogError(err)
			return nil, errors.New("Can not check storage")
		}
		arr[tray] = count
	}
	return arr, nil
}

func (s *PostgresService) PopFromTray(tray string) error {
	query := "select c.id from tray t join tray_batch tb on tb.tray = t.id join batch_cartridge bc on bc.batch = tb.batch join cartridge c on c.id = bc.cartridge where t.id = $1 order by c.id asc limit 1"
	var cId int
	err := s.db.QueryRow(query, tray).Scan(&cId)
	if err != nil {
		pgLogger.LogError(err)
		return errors.New("Can not pop cartridge")
	}
	query2 := "delete from cartridge where id = $1"
	_, err2 := s.db.Exec(query2, cId)
	if err2 != nil {
		pgLogger.LogError(err2)
		return errors.New("Can not pop cartridge")
	}
	return nil
}

func (s *PostgresService) ClearReserved() error {
	query := "update amount_items set reserved = 0, expired = 0 where reserved != 0 or expired != 0"
	_, err := s.db.Exec(query)
	if err != nil {
		pgLogger.LogError(err)
	}
	return nil
}

func (s *PostgresService) ClearBowlReserved(count int, itemCode int) error {
	query := "update amount_items set reserved = reserved-$1 where item_code = $2 and reserved > 0"
	_, err := s.db.Exec(query, count, itemCode)
	if err != nil {
		pgLogger.LogError(err)
	}
	return nil
}

func (s *PostgresService) UpdateExpired() error {
	q1 := "update amount_items set expired = 0"
	_, err := s.db.Exec(q1)
	if err != nil {
		pgLogger.LogError(err)
	}

	query := "select code,count(id) from (select i.code,c.id from batch b join tray_batch tb on tb.batch = b.id join batch_cartridge bc on bc.batch = b.id join cartridge c on bc.cartridge = c.id join item i on i.code = c.item_code where extract(DAY from now()-b.load_time)*24 + extract(HOUR from now()-b.load_time) >= i.storage_time group by tb.tray,c.id,b.load_time,i.code order by b.load_time) as sl group by sl.code"
	rows, err := s.db.Query(query)
	if err != nil {
		pgLogger.LogError(err)
		return err
	}
	for rows.Next() {
		var (
			code  int
			count int
		)
		err := rows.Scan(&code, &count)
		if err != nil {
			pgLogger.LogError(err)
		} else {
			q2 := "update amount_items set expired = $1 where item_code = $2"
			_, err := s.db.Exec(q2, count, code)
			if err != nil {
				pgLogger.LogError(err)
			}
		}
	}
	return nil
}
