package postgres

import (
	"time"
)

func (s *PostgresService) StartDaySession() (int, int) {
	var id int
	query := "select id from daysession where date(daytime)=$1"
	row := s.db.QueryRow(query, time.Now().Format("2006-01-02"))
	err := row.Scan(&id)
	if err != nil {
		query := "INSERT INTO daysession (daytime) VALUES ($1) returning id"
		row = s.db.QueryRow(query, time.Now().Format("2006-01-02 15:04:05"))
		err = row.Scan(&id)
		if err != nil {
			pgLogger.LogFatal(err)
		}
	}

	return id, 0
}

func (s *PostgresService) GetDayId(id int) (int, int) {
	var order_id int
	query := "select cur_order_id from daysession where id=$1"
	row := s.db.QueryRow(query, id)
	err := row.Scan(&order_id)
	if err != nil {
		pgLogger.LogFatal(err)
	}
	return order_id, 0
}

func (s *PostgresService) IncDayId(id int) error {
	query := "UPDATE daysession set cur_order_id=cur_order_id+1 where id=$1"
	_, err := s.db.Exec(query, id)
	if err != nil {
		pgLogger.LogFatal(err)
	}
	return err
}

func (s *PostgresService) GetTime() (time.Time, error) {
	query := "select * from now()"
	t := time.Time{}
	err := s.db.QueryRow(query).Scan(&t)
	if err != nil {
		pgLogger.LogError(err)
		return t, err
	}
	return t, nil
}
