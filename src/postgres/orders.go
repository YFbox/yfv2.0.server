package postgres

import (
	"errors"
	"fmt"
	api "server/API"
	"server/fillservice/media/bowls_img"
	"server/models"
	"strings"
	"time"
)

func (s *PostgresService) InsertOrder(SesDayId int, PartnerId int, bowlArr []*models.DbBowl, Total float32) (*models.DbOrder, error) {
	DayId, errCode := s.GetDayId(SesDayId)
	s.IncDayId(SesDayId)
	if errCode != 0 {
		pgLogger.LogFatal(errors.New(fmt.Sprintf("errorCode %d", errCode)))
	}

	if PartnerId == 0 {
		PartnerId = DayId
	}
	var oId int
	var oTime time.Time
	query := "INSERT INTO order_table(dayid,partner_id,total) VALUES ($1,$2,$3) returning id,order_time"
	err := s.db.QueryRow(query, DayId, PartnerId, Total).Scan(&oId, &oTime)
	if err != nil {
		pgLogger.LogFatal(errors.New(fmt.Sprintf("errorCode %d", err)))
	}
	ord := &models.DbOrder{
		Id:        oId,
		DayId:     DayId,
		PartnerId: PartnerId,
		Status:    models.OrderStatusPending,
		Total:     Total,
		OrderTime: oTime,
	}

	customCountArr := map[string]int{}
	customCode := map[string]int{}

	rows := []string{}
	for pos, bowl := range bowlArr {
		if bowl.CustomFlag || bowl.Modify {
			customCountArr[bowl.Name]++
			if customCountArr[bowl.Name] == 1 {
				img, _ := bowls_img.Asset("bowl/custom.png")
				bowl.Picture = img

				bowl.Code, err = s.InsertBowl(*bowl)
				if err != nil {
					s.DeleteOrder(oId)
					pgLogger.LogFatal(err)
					return nil, err
				}
				customCode[bowl.Name] = bowl.Code
			} else {
				bowl.Code = customCode[bowl.Name]
			}
		}
		rows = append(rows, fmt.Sprintf("(%d,%d,%d)", pos, ord.Id, bowl.Code))
		ord.Bowls = append(ord.Bowls, models.DbBowlStruct{Code: bowl.Code})
	}

	osquery := "INSERT INTO order_struct(position,order_id,bowl_code) VALUES " + strings.Join(rows, ",") + " returning id"
	osqrows, err2 := s.db.Query(osquery)
	if err2 != nil {
		s.DeleteOrder(oId)
		pgLogger.LogFatal(err)
		return nil, err2
	}
	iter := 0
	for osqrows.Next() {
		if iter < len(ord.Bowls) {
			err := osqrows.Scan(&ord.Bowls[iter].StructId)
			if err != nil {
				s.DeleteOrder(oId)
				pgLogger.LogFatal(err)
			}
			iter++
		} else {
			s.DeleteOrder(oId)
			pgLogger.LogFatal(err)
		}
	}
	return ord, nil
}

func (s *PostgresService) DeleteOrder(Id int) int {
	query := "DELETE FROM order_table WHERE id = $1"
	_, err := s.db.Exec(query, Id)
	if err != nil {
		pgLogger.LogError(err)
		return api.ErrorCanNotDeleteOrder
	}
	return 0
}

func (s *PostgresService) SetOrderStatus(Id int, Status string) int {
	strtime := ""
	switch Status {
	case models.OrderStatusCooking:
		strtime = "start_time"
		break
	case models.OrderStatusReady:
		strtime = "ready_time"
		break
	case models.OrderStatusClosed:
		strtime = "closed_time"
		break
	case models.OrderStatusErrClosed:
		strtime = "closed_time"
		break
	case models.OrderStatusCancelled:
		strtime = "closed_time"
		break
	}
	query := "UPDATE order_table SET status = $1," + strtime + " = now() WHERE id = $2"
	_, err := s.db.Exec(query, Status, Id)

	if err != nil {
		pgLogger.LogError(err)
		return api.ErrorCanNotSetOrderStatus
	}
	return 0
}

func (s *PostgresService) SetBowlStatus(StructId int, ModuleId int, Status string) (time.Time, int) {
	strtime := ""
	query := ""
	switch Status {
	case models.OrderStatusCooking:
		strtime = fmt.Sprintf("module_id = %d, start_time", ModuleId)
		query = "UPDATE order_struct SET " + strtime + " = now() WHERE id = $1 returning start_time"
		break
	case models.OrderStatusReady:
		query = "UPDATE order_struct SET ready_time = now() WHERE id = $1 returning ready_time"
		break
	case models.OrderStatusClosed:
		query = "UPDATE order_struct SET closed_time = now() WHERE id = $1 returning closed_time"
		break
	}

	var rt time.Time
	err := s.db.QueryRow(query, StructId).Scan(&rt)

	if err != nil {
		pgLogger.LogError(err)
		return rt, api.ErrorCanNotSetBowlStatus
	}
	return rt, 0
}

func (s *PostgresService) SetBowlStruct(StructId int, Cart *models.DbCartridge, Tray string) error {
	var price float32
	qp := "select price from item where code = $1"
	err := s.db.QueryRow(qp, Cart.ItemCode).Scan(&price)
	if err != nil {
		pgLogger.LogFatal(err)
	}
	query := "insert into order_cartridge (order_struct_id,cartridge_id,item_code,price,tray)values ($1,$2,$3,$4,$5)"
	_, err = s.db.Exec(query, StructId, Cart.Id, Cart.ItemCode, price, Tray)

	if err != nil {
		pgLogger.LogFatal(err)
	}
	return nil
}

func (s *PostgresService) GetOrderHistory() ([]*models.DbVerboseOrder, int) {
	ordArr := []*models.DbVerboseOrder{}

	query := "select ot.id,ot.order_time,os.id as struct_id,os.bowl_code,os.module_id,os.start_time,os.ready_time,os.closed_time from order_table ot join order_struct os on os.order_id = ot.id group by ot.id,order_time,os.id,os.bowl_code,os.module_id,os.start_time order by ot.order_time,ot.id,os.id,os.start_time"
	rows, err := s.db.Query(query)
	if err != nil {
		pgLogger.LogError(err)
		return nil, api.ErrorCanNotGetHistory
	}
	var prevOrd *models.DbVerboseOrder

	for rows.Next() {
		ord := &models.DbVerboseOrder{}
		os := &models.DbVerboseOrderStruct{}
		var bCode, errCode int
		err := rows.Scan(&ord.Id, &ord.OrderTime, &os.StructId, &bCode, &os.ModuleId, &os.StartTime, &os.ReadyTime, &os.CloseTime)
		if err != nil {
			pgLogger.LogError(err)
		}
		os.Bowl, errCode = s.GetBowl(bCode)
		if errCode != 0 {
			pgLogger.LogError(errors.New(fmt.Sprintf("error code %d", errCode)))
			return nil, api.ErrorCanNotGetHistory
		}
		cquery := "select cartridge_id,item_code,price,tray from order_cartridge where order_struct_id = $1"
		crows, cerr := s.db.Query(cquery, os.StructId)
		if cerr != nil {
			pgLogger.LogError(cerr)
			return nil, api.ErrorCanNotGetHistory
		}
		for crows.Next() {
			cart := models.DbVerboseCartridge{}
			err := crows.Scan(&cart.Code, &cart.ItemCode, &cart.Price, &cart.Tray)
			if err == nil {
				os.Cartridges = append(os.Cartridges, cart)
			}

		}

		if prevOrd != nil {
			if prevOrd.Id != ord.Id {
				ordArr = append(ordArr, prevOrd)
				ord.Bowls = append(ord.Bowls, os)
				prevOrd = ord
			} else {
				prevOrd.Bowls = append(prevOrd.Bowls, os)
			}
		} else {
			ord.Bowls = append(ord.Bowls, os)
			prevOrd = ord
		}

	}
	if prevOrd != nil {
		ordArr = append(ordArr, prevOrd)
	}
	return ordArr, 0
}
