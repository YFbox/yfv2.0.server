package dbservice

import (
	"errors"
	"strconv"
	"time"

	"server/models"

	"math/rand"
	"server/postgres"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var logger, _ = zap.Config{
	Encoding:    "json",
	Level:       zap.NewAtomicLevelAt(zapcore.InfoLevel),
	OutputPaths: []string{"stdout", "../log/db.log"},
}.Build()

var sugar = logger.Sugar()

var Db = postgres.PostgresService{}

func OpenConn() error {
	err := Db.InitService()

	if err != nil {
		sugar.Errorw("Failed connect to the database",
			"error", "dbservice",
			err, "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return err
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"Connected to Database", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	return nil
}

func InitFromFile(name string) error {
	err := db.InitFromFile(name)

	if err != nil {
		sugar.Errorw("Failed connect to the database",
			"error", "dbservice",
			err, "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"Database loaded from "+name, "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	return err
}

func DbStartDaySession() (string, error) {
	id, err := db.StartDaySession()
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "StartDaySession",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return id, err
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command:", " StartDaySession",
		"id: "+id, "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	return id, nil
}

func DbGetDayId(id string) (string, error) {
	id, err := db.GetDayId(id)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "GetDayId",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return id, err
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command: GetDayId", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	return id, nil
}

func DbIncDayId(id string) error {
	err := db.IncDayId(id)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "IncDayId",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return err
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command: IncDayId", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	return nil
}

func DbAddItem(Code int, Title string, Type string, Description string, StorageTime int, Allergy string, Price int) error {
	i := models.DbItem{Code, Title, Type, Description, StorageTime, Allergy, Price}
	err := db.AddItem(i)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "AddItem",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "AddItem",
			"Item "+i.Title+" added", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbDeleteItem(Title string) error {
	err := db.DeleteItem(Title)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "DeleteItem",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "DeleteItem",
			"Item "+Title+" deleted", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbGetItems() (*models.ResponseGetItemsData, error) {
	data, err := db.GetItems()
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "GetItems",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "GetItems",
			"Items obtained", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return data, err
}

func DbCheckAmountItems(item string, count int) bool {
	trays, err := db.GetItemTrays(item)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "AddTray",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return false
	}
	storageAmount := 0

	for _, t := range trays {
		_, am, err2 := db.GetValueOfTray(t)
		if err2 != nil {
			sugar.Errorw("connect to the database",
				"error", "dbservice",
				"command", "AddTray",
				"err", err,
				"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
			return false
		}
		storageAmount += am
	}

	if storageAmount >= count {
		return true
	}
	return false
}

func DbAddTray(Id string, Active bool) error {
	err := db.AddTray(Id, Active)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "AddTray",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "AddTray",
			"Tray "+Id+" added", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbDeleteTray(Id string) error {
	err := db.DeleteTray(Id)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "DeleteTray",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "DeleteTray",
			"Tray "+Id+" deleted", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbGetTrays() (*models.ResponseGetStorageData, error) {
	tr, err := db.GetTrays()
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "GetTrays",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command", "GetTrays",
		"Trays obtained", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	return tr, err
}
func DbGetTrayByItem(Item string) (string, error) {
	tr, err := db.GetTrayByItem(Item)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "GetTrayByItem",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return tr, err
}
func DbPickUpItem(Item string) (*models.DbTray, error) {
	id, err := DbGetTrayByItem(Item)
	if err != nil {
		return nil, err
	}
	tr, err2 := DbPopOneFromTray(id)
	return tr, err2
}
func DbLoadTray(Id string, Item string, Amount int) (*models.DbTray, error) {
	curI, curA, err := db.GetValueOfTray(Id)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "LoadTray",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return nil, models.ErrorWrongTrayData
	}
	if (curA != 0 && curI == Item) || curA == 0 {
		tr, err := db.LoadTray(Id, Item, Amount)
		if err != nil {
			sugar.Errorw("connect to the database",
				"error", "dbservice",
				"command", "LoadTray",
				"err", err,
				"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
			return nil, models.ErrorWrongTrayData
		}
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "LoadTray",
			"Tray "+Id+" loaded by "+strconv.Itoa(Amount)+" "+Item, "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return tr, nil
	}
	return nil, models.ErrorWrongTrayData
}
func DbPopOneFromTray(Id string) (*models.DbTray, error) {
	_, am, err := db.GetValueOfTray(Id)

	if err == nil && am > 0 {
		tr, err := db.PopOneFromTray(Id)
		if err != nil {
			sugar.Errorw("connect to the database",
				"error", "dbservice",
				"command", "PopOneFromTray",
				"err", err,
				"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
			return nil, models.ErrorNoItemsInTray
		}
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "PopOneFromTray",
			"From tray "+Id+" deleted 1 item", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return tr, err
	}
	return nil, models.ErrorNoItemsInTray
}

func DbClearTray(Id string) (*models.DbTray, error) {
	tr, err := db.ClearTray(Id)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "ClearTray",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return nil, models.ErrorWrongTrayData
	}

	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command", "ClearTray",
		"Tray "+Id+" cleared", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	return tr, nil
}

func DbAddBowl(Name string, Price int, cookingTime time.Time, Picture []byte) error {
	err := db.AddBowl(Name, Price, cookingTime, Picture)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "AddBowl",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "AddBowl",
			"Bowl "+Name+" added", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbDeleteBowl(Name string) error {
	err := db.DeleteBowl(Name)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "DeleteBowl",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "DeleteBowl",
			"Bowl "+Name+" deleted", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbGetBowls() (*models.ResponseGetBowlsData, error) {
	data, err := db.GetBowls()
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "GetBowls",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "GetBowls",
			"Bowls obtained", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return data, err
}

func DbAddRecipe(BowlName string, Items []string) error {
	err := db.AddRecipe(BowlName, Items)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "AddRecipe",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "AddRecipe",
			"Recipe "+BowlName+" added", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbDeleteRecipe(Name string) error {
	err := db.DeleteRecipe(Name)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "DeleteRecipe",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "DeleteRecipe",
			"Recipe "+Name+" deleted", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbCheckOrder(httpBowls []models.CNODBowl) error {
	if len(httpBowls) == 0 {
		return models.ErrorUnknownRecipe
	}

	items := map[string]int{}
	for _, b := range httpBowls {
		if !b.CustomFlag {
			recipe, err := db.GetRecipe(b.Name)
			if err != nil {
				sugar.Errorw("connect to the database",
					"error", "dbservice",
					"command", "GetRecipe",
					"err", err,
					"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
				return models.ErrorUnknownRecipe
			}
			for _, title := range recipe {
				items[title] += b.Count
			}
		} else {
			for _, i := range b.Items {
				items[i.Title] += i.Count
			}
		}
	}
	for i, count := range items {
		if !DbCheckAmountItems(i, count) {
			return errors.New(models.ErrorNotEnoughItems.Error() + ": " + i)
		}
	}
	return nil
}

func DbAddOrder(SesDayId string, Total float32, httpBowls []models.CNODBowl) (*models.DbOrder, error) {

	Bowls := []string{}
	for _, b := range httpBowls {
		for i := b.Count; i > 0; i-- {
			Bowls = append(Bowls, b.Name)
		}
		if b.CustomFlag {
			items := []string{}
			price := 0
			cookingTime := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 2, 30, 0, time.Local)
			for _, it := range b.Items {
				for pos := 0; pos < it.Count; pos++ {
					items = append(items, it.Title)
					price += 30
				}
			}
			err := db.AddBowl(b.Name, price, cookingTime, nil)
			if err != nil {
				sugar.Errorw("connect to the database",
					"error", "dbservice",
					"command", "AddCustomBowl",
					"err", err,
					"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
				return nil, models.ErrorWrongCustomRecipe
			}
			err = db.AddRecipe(b.Name, items)
			if err != nil {
				sugar.Errorw("connect to the database",
					"error", "dbservice",
					"command", "AddCustomRecipe",
					"err", err,
					"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
				return nil, models.ErrorWrongCustomRecipe
			}
		}
	}

	o, err := db.AddOrder(SesDayId, Total, Bowls)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "AddOrder",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return nil, models.ErrorWrongOrderData
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command", "AddOrder",
		"Order "+o.Id+" added", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	return o, nil
}
func DbDeleteOrder(Id string) error {
	err := db.DeleteOrder(Id)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "DeleteOrder",
			"err", err,
			"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	} else {
		sugar.Infow("connect to the database",
			"info", "dbservice",
			"command", "DeleteOrder",
			"Order "+Id+" deleted", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	return err
}
func DbStartOrder(o *models.MemOrder) error {
	t := time.Now()

	o.Start_time = t
	o.Status = models.OrderStatusCooking

	err := db.SetOrderStatus(o.Id, models.OrderStatusCooking, t)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "SetOrderStatus",
			"err", err,
			"time", strconv.Itoa(t.Hour())+":"+strconv.Itoa(t.Minute()))
		return err
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command", "SetOrderStatus",
		"Order "+o.Id+" set to cooking", "time: "+strconv.Itoa(t.Hour())+":"+strconv.Itoa(t.Minute()))

	return nil
}
func DbStopOrder(o *models.MemOrder) error {
	t := time.Now()

	o.Ready_time = t
	o.Status = models.OrderStatusReady

	err := db.SetOrderStatus(o.Id, models.OrderStatusReady, t)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "SetOrderStatus",
			"err", err,
			"time", strconv.Itoa(t.Hour())+":"+strconv.Itoa(t.Minute()))
		return err
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command", "SetOrderStatus",
		"Order "+o.Id+" set to ready", "time: "+strconv.Itoa(t.Hour())+":"+strconv.Itoa(t.Minute()))

	return nil
}
func DbCloseOrder(o *models.MemOrder) error {
	t := time.Now()

	o.Closed_time = t
	o.Status = models.OrderStatusClosed

	err := db.SetOrderStatus(o.Id, models.OrderStatusClosed, t)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "SetOrderStatus",
			"err", err,
			"time", strconv.Itoa(t.Hour())+":"+strconv.Itoa(t.Minute()))
		return err
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command", "SetOrderStatus",
		"Order "+o.Id+" set to closed", "time: "+strconv.Itoa(t.Hour())+":"+strconv.Itoa(t.Minute()))

	return nil
}
func DbCloseWithErrOrder(o *models.DbOrder) error {
	t := time.Now()

	o.Closed_time = t
	o.Status = models.OrderStatusErrClosed

	err := db.SetOrderStatus(o.Id, models.OrderStatusErrClosed, t)
	if err != nil {
		sugar.Errorw("connect to the database",
			"error", "dbservice",
			"command", "SetOrderStatus",
			"err", err,
			"time", strconv.Itoa(t.Hour())+":"+strconv.Itoa(t.Minute()))
		return err
	}
	sugar.Infow("connect to the database",
		"info", "dbservice",
		"command", "SetOrderStatus",
		"Order "+o.Id+" set to closed with error", "time: "+strconv.Itoa(t.Hour())+":"+strconv.Itoa(t.Minute()))

	return nil
}
func DbGetMemOrder(ord *models.DbOrder) (*models.MemOrder, error) {
	if ord != nil {
		memord := &models.MemOrder{
			Id:         ord.Id,
			DayId:      ord.DayId,
			Status:     models.OrderStatusPending,
			Order_time: ord.Order_time,
		}
		bowls, err := db.GetOrderStruct(ord.Id)
		if err != nil {
			sugar.Errorw("connect to the database",
				"error", "dbservice",
				"command", "GetOrderStruct",
				"err", err,
				"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
			return nil, models.ErrorNoSuchOrder
		}
		for _, b := range bowls {
			t, err := db.GetCookingTime(b)
			if err != nil {
				sugar.Errorw("connect to the database",
					"error", "dbservice",
					"command", "GetCookingTime",
					"err", err,
					"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

				return nil, models.ErrorUnknownBowl
			}
			membowl := &models.MemBowl{
				Name:         b,
				Status:       models.OrderStatusPending,
				Order:        memord,
				Id:           rand.Uint32() / 65536,
				Cooking_time: t,
			}
			recipe, err := db.GetRecipe(b)
			if err != nil {
				sugar.Errorw("connect to the database",
					"error", "dbservice",
					"command", "GetRecipe",
					"err", err,
					"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
				return nil, models.ErrorUnknownRecipe
			}
			for _, title := range recipe {

				memitem := &models.MemItem{
					Name:   title,
					Status: models.OrderStatusPending,
				}
				membowl.Items = append(membowl.Items, memitem)
			}
			memord.Bowls = append(memord.Bowls, membowl)
		}
		return memord, nil
	}
	sugar.Infow("connect to the database",
		"error", "dbservice",
		"command", "GetMemOrder",
		"No data to get memory order", "time: "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))

	return nil, models.ErrorEmptyOrder

}
