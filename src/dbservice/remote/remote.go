package remote

import (
	"database/sql"
	"fmt"
	"server/models"
	"server/postgres"
)

var db = postgres.PostgresService{}

func InitService() error {
	var err error
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME)
	db.db, err = sql.Open("postgres", dbinfo)
	if err != nil {
		return err
	}

	if err = Db.db.Ping(); err != nil {
		return err
	}
	return nil
}
func OpenConn() error {
	err := InitService()

	return err
}

//InitFromFile ..
func InitFromFile(name string) error {
	err := db.InitFromFile(name)

	return err
}

func DbAddItem(Code int, Title string, Type string, Description string, StorageTime int, Allergy string, Price int) error {
	i := models.DbItem{Code, Title, Type, Description, StorageTime, Allergy, Price}
	err := db.AddItem(i)
	return err
}
func DbDeleteItem(Title string) error {
	err := db.DeleteItem(Title)
	return err
}
func DbGetItems() (*models.ResponseGetItemsData, error) {
	data, err := db.GetItems()

	return data, err
}

func DbAddTray(Id string, Active bool) error {
	err := db.AddTray(Id, Active)

	return err
}
func DbDeleteTray(Id string) error {
	err := db.DeleteTray(Id)

	return err
}
func DbLoadTray(Id string, Item string, Amount int) (*models.DbTray, error) {
	tr, err := db.LoadTray(Id, Item, Amount)

	return tr, err
}
func DbPopOneFromTray(Id string) (*models.DbTray, error) {
	tr, err := db.PopOneFromTray(Id)

	return tr, err
}
func DbClearTray(Id string) (*models.DbTray, error) {
	tr, err := db.ClearTray(Id)

	return tr, err
}

func DbAddBowl(Name string, Price int, Picture []byte) error {
	err := db.AddBowl(Name, Price, Picture)

	return err
}
func DbDeleteBowl(Name string) error {
	err := db.DeleteBowl(Name)

	return err
}
func DbGetBowls() (*models.ResponseGetBowlsData, error) {
	data, err := db.GetBowls()

	return data, err
}

func DbAddRecipe(BowlName string, Items map[int]string) error {
	err := db.AddRecipe(BowlName, Items)

	return err
}
func DbDeleteRecipe(Name string) error {
	err := db.DeleteRecipe(Name)

	return err
}

func DbAddOrder(Total float32, rBowl []models.CNODBowl) (*models.DbOrder, error) {
	Bowls := map[int]string{}
	id := 1
	for _, b := range rBowl {
		for i := b.Count; i > 0; i-- {
			Bowls[id] = b.Name
			id++
		}
	}

	o, err := db.AddOrder(Total, Bowls)
	return o, err
}
func DbDeleteOrder(Id string) error {
	err := db.DeleteOrder(Id)

	return err
}
func DbStartOrder(Id string) error {
	err := db.SetOrderStatus(Id, models.OrderStatusCooking)

	return err
}
func DbStopOrder(Id string) error {
	err := db.SetOrderStatus(Id, models.OrderStatusReady)

	return err
}
func DbGetMemOrder(ord *models.DbOrder) (*models.MemOrder, error) {
	memord := &models.MemOrder{
		Id:     ord.Id,
		Status: models.OrderStatusPending,
	}
	bowls, err := db.GetOrderStruct(ord.Id)
	for idx, b := range bowls {
		membowl := &models.MemBowl{
			Name:   b,
			Status: models.OrderStatusPending,
			Order:  memord,
		}
		recipe, _ := db.GetRecipe(b)
		for pos, it := range recipe {
			tray, _ := db.GetItemTray(it)
			memitem := &models.MemItem{it, tray, models.OrderStatusPending}
			membowl.Items[pos] = memitem
		}
		memord.Bowls[idx] = membowl
	}
	return memord, err
}

func MakeBackup() error {
	f := func(name string) {
		q := "copy " + name + " to '/home/csv_backup/" + name + ".csv' csv header"
		_, err := s.db.Exec(q)
		if err != nil {
			pgLogger.LogError(err)
		}
	}
	list := []string{
		"amount_bowls",
		"amount_items",
		"batch",
		"batch_cartridge",
		"cartridge",
		"daysession",
		"order_cartridge",
		"order_struct",
		"order_table",
		"recipe",
		"tray",
		"tray_batch",
	}
	for _, el := range list {
		f(el)
	}
	q := "copy (select code,name,bowl_type,price,cooking_time,custom_flag,is_menu_flag,modified from bowl) to '/home/csv_backup/bowl.csv' csv header"
	_, err := s.db.Exec(q)
	if err != nil {
		pgLogger.LogError(err)
	}
	q = "copy (select code,title,item_type,throw_speed,item_description,storage_time,allergy,price from item) to '/home/csv_backup/item.csv' csv header"
	_, err = s.db.Exec(q)
	if err != nil {
		pgLogger.LogError(err)
	}
	return nil
}
