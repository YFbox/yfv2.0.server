package main

import (
	"bufio"
	"fmt"
	"os"

	"../canservice"
	"../models"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	cm := canservice.NewCanManager("/dev/ttyACM0")
	if cm != nil {
		go cm.Manage()
		go Process(cm)
		b1 := models.TypeBowl{}
		b1.ID = 1
		b1.Items = []models.TypeItem{
			models.TypeItem{TrayID: 0xA6},
			models.TypeItem{TrayID: 0xC3},
		}

	LOOP:
		for {
			fmt.Printf("Enter key:\n-x: Exit\n-s: Send\n")
			scanner.Scan()
			key := scanner.Text()
			switch key {
			case "x":
				break LOOP
			case "s":
				cm.SendBowl(0x3, b1)
				// 	fmt.Printf("Enter ID\n")
				// 	scanner.Scan()
				// 	id := scanner.Text()
				// 	fmt.Printf("Enter DLC\n")
				// 	scanner.Scan()
				// 	dlc := scanner.Text()
				// 	fmt.Printf("Enter data\n")
				// 	scanner.Scan()
				// 	data := scanner.Text()
				// 	cm.Send(canservice.CanMsg{Id: id, Dlc: dlc, Data: data})
			}

		}
	}
}

func Process(cm *canservice.CanManager) {
	for {
		select {
		case ms := <-cm.Msg:
			fmt.Println("mes: ", ms)
		}
	}
}
