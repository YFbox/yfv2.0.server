package fillservice

import "server/models"

func GetBowlList(items map[string]*models.DbItem) map[string]*models.DbBowl {
	return map[string]*models.DbBowl{
		"Salmon Poke Bowl": &models.DbBowl{
			CookingTime: 60,
			Name:        "Salmon Poke Bowl",
			Price:       490,
			Items: []models.DbRecipeItem{
				models.DbRecipeItem{
					ItemCode: items["Sticky rice"].Code,
					Count:    2,
				},

				models.DbRecipeItem{
					ItemCode: items["Red cabbage in sweet chili"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Avocado & cucumber"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Cherry tomatoes"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Salmon"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Radish slices and herbs"].Code,
					Count:    1,
				},
			},
			Type:       "salad",
			ShowInMenu: true,
		},
		"Salmon Tuna Poke Special": &models.DbBowl{
			CookingTime: 60,
			Name:        "Salmon Tuna Poke Special",
			Price:       760,
			Items: []models.DbRecipeItem{
				models.DbRecipeItem{
					ItemCode: items["Sticky rice"].Code,
					Count:    2,
				},

				models.DbRecipeItem{
					ItemCode: items["Red cabbage in sweet chili"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Edamame beans"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Orange teriyaki"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Salmon Ceviche"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Tuna Ceviche"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Radish slices and herbs"].Code,
					Count:    1,
				},
			},
			Type:       "salad",
			ShowInMenu: true,
		},
		"Salmon Ceviche Poke Bowl": &models.DbBowl{
			CookingTime: 60,
			Name:        "Salmon Ceviche Poke Bowl",
			Price:       490,
			Items: []models.DbRecipeItem{
				models.DbRecipeItem{
					ItemCode: items["Sticky rice"].Code,
					Count:    2,
				},

				models.DbRecipeItem{
					ItemCode: items["Red cabbage in sweet chili"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Avocado & cucumber"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Cherry tomatoes"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Salmon Ceviche"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Radish slices and herbs"].Code,
					Count:    1,
				},
			},
			Type:       "salad",
			ShowInMenu: true,
		},
		"Tuna Poke Bowl": &models.DbBowl{
			CookingTime: 60,
			Name:        "Tuna Poke Bowl",
			Price:       490,
			Items: []models.DbRecipeItem{
				models.DbRecipeItem{
					ItemCode: items["Quinoa"].Code,
					Count:    2,
				},

				models.DbRecipeItem{
					ItemCode: items["Edamame beans"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Avocado & cucumber"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Cherry tomatoes"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Tuna"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Radish slices and herbs"].Code,
					Count:    1,
				},
			},
			Type:       "salad",
			ShowInMenu: true,
		},
		"Tuna Сeviche Poke Bowl": &models.DbBowl{
			CookingTime: 60,
			Name:        "Tuna Сeviche Poke Bowl",
			Price:       490,
			Items: []models.DbRecipeItem{
				models.DbRecipeItem{
					ItemCode: items["Quinoa"].Code,
					Count:    2,
				},

				models.DbRecipeItem{
					ItemCode: items["Edamame beans"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Avocado & cucumber"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Cherry tomatoes"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Tuna Ceviche"].Code,
					Count:    1,
				},
				models.DbRecipeItem{
					ItemCode: items["Radish slices and herbs"].Code,
					Count:    1,
				},
			},
			Type:       "salad",
			ShowInMenu: true,
		},
	}
}

/*
bArr["Salmon Ceviche"] = []models.DbRecipeItem{
	models.DbRecipeItem{
		ItemCode: items["Kale"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Avocado & cucumber"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Salmon"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Radish slices and herbs"],
		Count:    1,
	},
}
bArr["Fresh Veggie Salad"] = []models.DbRecipeItem{
	models.DbRecipeItem{
		ItemCode: items["Greens mix"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Cucumber cubes"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Tomatoes"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Goat cheese"],
		Count:    1,
	},
}
bArr["Chicken Caesar"] = []models.DbRecipeItem{
	models.DbRecipeItem{
		ItemCode: items["Greens mix"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Green Leaf Lettuce"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Boiled quail eggs"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Roasted Chicken"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Cherry tomatoes"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Goat cheese"],
		Count:    1,
	},
}
bArr["Chicken Cobb"] = []models.DbRecipeItem{
	models.DbRecipeItem{
		ItemCode: items["Green Leaf Lettuce"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Carrot cubes"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Avocado & cucumber"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Boiled quail eggs"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Cherry tomatoes"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Roasted Chicken"],
		Count:    1,
	},
}
bArr["Harvest Bowl"] = []models.DbRecipeItem{
	models.DbRecipeItem{
		ItemCode: items["Kale"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Brown rice"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Roasted Sweet Potato"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Goat cheese"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Roasted Chicken"],
		Count:    1,
	},
	models.DbRecipeItem{
		ItemCode: items["Apple cubes"],
		Count:    1,
	},
}*/
