CREATE EXTENSION IF NOT EXISTS citext;

CREATE TYPE itype AS ENUM ('base', 'protein', 'veggies','dressings','topping','uncknown');
CREATE TYPE btype AS ENUM ('salad','soup','uncknown');
CREATE TYPE ostatustype AS ENUM ('pending','loading','loaded' ,'cooking', 'ready','closed','errclosed','cancelled');

CREATE TABLE IF NOT EXISTS item(
  code bigserial PRIMARY KEY,       
  title CITEXT NOT NULL UNIQUE,     
  item_type itype NOT NULL,
  throw_speed int DEFAULT 0,           
  item_description CITEXT DEFAULT '',  
  storage_time integer DEFAULT 0,
  allergy CITEXT DEFAULT '',
  price float DEFAULT 0,
  picture bytea
);

CREATE TABLE IF NOT EXISTS amount_items( 
  item_code integer REFERENCES item(code) ON DELETE CASCADE ON UPDATE CASCADE,
  amount integer DEFAULT 0,
  reserved integer DEFAULT 0,
  expired integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS cartridge(
  id bigserial PRIMARY KEY,        
  item_code integer REFERENCES item(code) ON DELETE CASCADE ON UPDATE CASCADE,
  load_time timestamp with time zone
);

CREATE TABLE IF NOT EXISTS tray(
  id CITEXT PRIMARY KEY,  
  active boolean DEFAULT true not NULL
);

CREATE TABLE IF NOT EXISTS batch(
  id bigserial PRIMARY KEY,   
  load_time timestamp with time zone DEFAULT now() 
);

CREATE TABLE IF NOT EXISTS tray_batch(
  tray CITEXT REFERENCES tray(id) ON DELETE CASCADE ON UPDATE CASCADE,  
  batch bigserial REFERENCES batch(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS batch_cartridge(
  batch bigserial REFERENCES batch(id) ON DELETE CASCADE ON UPDATE CASCADE,  
  cartridge bigserial REFERENCES cartridge(id) ON DELETE CASCADE ON UPDATE CASCADE  
);


CREATE TABLE IF NOT EXISTS bowl( 
  code bigserial PRIMARY KEY,
  name CITEXT not NULL,
  bowl_type btype not NULL,       
  price float DEFAULT 0,
  cooking_time integer , 
  custom_flag boolean DEFAULT false,
  is_menu_flag boolean DEFAULT false, 
  modified boolean DEFAULT false,  
  picture bytea
);

CREATE TABLE IF NOT EXISTS amount_bowls( 
  bowl_code integer REFERENCES bowl(code) ON DELETE CASCADE ON UPDATE CASCADE,
  amount integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS recipe( 
  id bigserial PRIMARY KEY, 
  bowl_code integer REFERENCES bowl(code) ON DELETE CASCADE ON UPDATE CASCADE, 
  position integer NOT NULL,
  item_code integer REFERENCES item(code) ON DELETE CASCADE ON UPDATE CASCADE,
  count integer not null       
);

CREATE TABLE IF NOT EXISTS order_table(
  id bigserial PRIMARY KEY,
  dayid integer NOT NULL,
  partner_id CITEXT NOT NULL,
  order_time timestamp with time zone DEFAULT now(), 
  start_time timestamp with time zone,
  ready_time timestamp with time zone,
  closed_time timestamp with time zone,  
  status ostatustype DEFAULT 'pending',
  total float DEFAULT 0
);

CREATE TABLE IF NOT EXISTS order_struct(
  id bigserial PRIMARY KEY,
  position integer NOT NULL,
  order_id bigserial REFERENCES order_table(id) ON DELETE CASCADE ON UPDATE CASCADE, 
  bowl_code integer REFERENCES bowl(code) ON DELETE CASCADE ON UPDATE CASCADE,
  module_id integer DEFAULT 0,
  start_time timestamp with time zone,
  ready_time timestamp with time zone,
  closed_time timestamp with time zone
);

CREATE TABLE IF NOT EXISTS order_cartridge(
  order_struct_id bigserial REFERENCES order_struct(id) ON DELETE CASCADE ON UPDATE CASCADE, 
  cartridge_id integer DEFAULT 0,
  item_code integer REFERENCES item(code) ON DELETE CASCADE ON UPDATE CASCADE,
  price float DEFAULT 0,
  tray CITEXT
);

CREATE TABLE IF NOT EXISTS daysession(
  id bigserial PRIMARY KEY,
  cur_order_id integer DEFAULT 1,
  daytime timestamp with time zone DEFAULT now()
);


CREATE FUNCTION item_insert() RETURNS trigger AS '
BEGIN
insert into amount_items (item_code) values (NEW.code);
return NEW;
END;
' 
LANGUAGE  plpgsql;

CREATE TRIGGER item_trig_insert
AFTER INSERT ON item FOR EACH ROW
EXECUTE PROCEDURE  item_insert();

CREATE FUNCTION bowl_insert() RETURNS trigger AS '
BEGIN
insert into amount_bowls (bowl_code) values (NEW.code);
return NEW;
END;
' 
LANGUAGE  plpgsql;

CREATE TRIGGER bowl_trig_insert
AFTER INSERT ON bowl FOR EACH ROW
EXECUTE PROCEDURE  bowl_insert();

CREATE FUNCTION bowl_amount_update() RETURNS trigger AS '
BEGIN
with sla as (select bowl_code,min(amount) as available from (select bowl_code,(ai.amount-ai.reserved-ai.expired)/(rc.count) as amount from recipe rc left join amount_items ai on ai.item_code = rc.item_code group by bowl_code,position,rc.item_code,ai.amount,ai.reserved,ai.expired,rc.count order by bowl_code,position) as sl group by sl.bowl_code)
update amount_bowls as ab set amount = sla.available from sla where ab.bowl_code = sla.bowl_code;
return NEW;
END;
' 
LANGUAGE  plpgsql;

CREATE TRIGGER bowl_amount_trig_update
AFTER UPDATE OR INSERT ON amount_items FOR EACH ROW
EXECUTE PROCEDURE  bowl_amount_update();

CREATE TRIGGER bowl_amount_trig_recipe_update
AFTER UPDATE OR INSERT ON recipe FOR EACH ROW
EXECUTE PROCEDURE  bowl_amount_update();

CREATE FUNCTION cartridge_insert() RETURNS trigger AS '
BEGIN
update amount_items set amount = amount + 1 where item_code = NEW.item_code;
IF NOT FOUND THEN
        insert into amount_items (item_code,amount) values(NEW.item_code,1);
END IF;
return NEW;
END;
' 
LANGUAGE  plpgsql;

CREATE TRIGGER cartridge_trig_insert
AFTER INSERT ON cartridge FOR EACH ROW
EXECUTE PROCEDURE  cartridge_insert();

CREATE FUNCTION cartridge_delete() RETURNS trigger AS '
BEGIN
update amount_items set amount = amount - 1 where item_code = OLD.item_code and amount > 0;
with sl as (select id,count(bc.cartridge) from batch b left join batch_cartridge bc on bc.batch = id group by id, bc.cartridge)
delete from batch where id in (select id from sl where count = 0);
return OLD;
END;
' 
LANGUAGE  plpgsql;

CREATE TRIGGER cartridge_trig_delete
AFTER DELETE ON cartridge FOR EACH ROW
EXECUTE PROCEDURE  cartridge_delete();

CREATE FUNCTION batch_cartridge_delete() RETURNS trigger AS '
BEGIN
delete from cartridge where id = OLD.cartridge;
return OLD;
END;
' 
LANGUAGE  plpgsql;

CREATE TRIGGER batch_cartridge_trig_delete
AFTER DELETE ON batch_cartridge FOR EACH ROW
EXECUTE PROCEDURE  batch_cartridge_delete();

CREATE FUNCTION tray_batch_delete() RETURNS trigger AS '
BEGIN
delete from batch where id = OLD.batch;
return OLD;
END;
' 
LANGUAGE  plpgsql;

CREATE TRIGGER tray_batch_trig_delete
AFTER DELETE ON tray_batch FOR EACH ROW
EXECUTE PROCEDURE  tray_batch_delete();
