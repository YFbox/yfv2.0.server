drop type if EXISTS ostatustype CASCADE;
drop type if EXISTS itype CASCADE;
drop type if EXISTS btype CASCADE;

drop table if EXISTS item CASCADE;
drop table if EXISTS amount_items CASCADE;

drop table if EXISTS cartridge CASCADE;
drop table if EXISTS tray CASCADE;
drop table if EXISTS tray_batch CASCADE;
drop table if EXISTS batch CASCADE;
drop table if EXISTS batch_cartridge CASCADE;

drop table if EXISTS bowl CASCADE;
drop table if EXISTS amount_bowls CASCADE;
drop table if EXISTS recipe CASCADE;

drop table if EXISTS order_table CASCADE;
drop table if EXISTS order_struct CASCADE;
drop table if EXISTS order_cartridge CASCADE;
drop table if EXISTS daysession CASCADE;

drop function if exists  item_insert() CASCADE;
drop trigger if exists  item_trig_insert on item CASCADE;

drop function if exists  bowl_insert() CASCADE;
drop trigger if exists  bowl_trig_insert on bowl CASCADE;

drop function if exists  bowl_amount_update() CASCADE;
drop trigger if exists  bowl_amount_trig_update on amount_items CASCADE;
drop trigger if exists  bowl_amount_trig_recipe_update on recipe CASCADE;

drop function if exists cartridge_insert() CASCADE;
drop trigger if exists  cartridge_trig_insert on cartridge CASCADE;

drop function if exists cartridge_delete() CASCADE;
drop trigger if exists  cartridge_trig_delete on cartridge CASCADE;

drop function if exists batch_cartridge_delete() cascade;
drop function if exists tray_batch_delete() cascade;