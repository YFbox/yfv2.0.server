package fillservice

import (
	"server/models"
)

func GetItemList() map[string]*models.DbItem {
	return map[string]*models.DbItem{
		"Sticky rice": &models.DbItem{
			Title:       "Sticky rice",
			Description: "Description",
			Price:       40,
			StorageTime: 12,
			Type:        "base",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Red cabbage in sweet chili": &models.DbItem{
			Title:       "Red cabbage in sweet chili",
			Description: "Description",
			Price:       30,
			StorageTime: 12,
			Type:        "veggies",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Avocado & cucumber": &models.DbItem{
			Title:       "Avocado & cucumber",
			Description: "Description",
			Price:       60,
			StorageTime: 12,
			Type:        "veggies",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Cherry tomatoes": &models.DbItem{
			Title:       "Cherry tomatoes",
			Description: "Description",
			Price:       30,
			StorageTime: 12,
			Type:        "veggies",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Salmon": &models.DbItem{
			Title:       "Salmon",
			Description: "Description",
			Price:       270,
			StorageTime: 12,
			Type:        "protein",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Radish slices and herbs": &models.DbItem{
			Title:       "Radish slices and herbs",
			Description: "Description",
			Price:       20,
			StorageTime: 12,
			Type:        "topping",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Quinoa": &models.DbItem{
			Title:       "Quinoa",
			Description: "Description",
			Price:       40,
			StorageTime: 12,
			Type:        "base",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Edamame beans": &models.DbItem{
			Title:       "Edamame beans",
			Description: "Description",
			Price:       30,
			StorageTime: 12,
			Type:        "veggies",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Tuna": &models.DbItem{
			Title:       "Tuna",
			Description: "Description",
			Price:       270,
			StorageTime: 12,
			Type:        "protein",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Eel in unagi sauce": &models.DbItem{
			Title:       "Eel in unagi sauce",
			Description: "Description",
			Price:       490,
			StorageTime: 12,
			Type:        "protein",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Tuna Ceviche": &models.DbItem{
			Title:       "Tuna Ceviche",
			Description: "Description",
			Price:       270,
			StorageTime: 12,
			Type:        "protein",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Orange teriyaki": &models.DbItem{
			Title:       "Orange teriyaki",
			Description: "Description",
			Price:       60,
			StorageTime: 12,
			Type:        "veggies",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Salmon Ceviche": &models.DbItem{
			Title:       "Salmon Ceviche",
			Description: "Description",
			Price:       270,
			StorageTime: 12,
			Type:        "protein",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Tuna Kimchi": &models.DbItem{
			Title:       "Tuna Kimchi",
			Description: "Description",
			Price:       270,
			StorageTime: 12,
			Type:        "protein",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		"Salmon Kimchi": &models.DbItem{
			Title:       "Salmon Kimchi",
			Description: "Description",
			Price:       270,
			StorageTime: 12,
			Type:        "protein",
			Allergy:     "No allergy",
			ThrowSpeed:  0,
		},

		/*
			"Kale":&models.DbItem{
				Title:       "Kale",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "base",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Greens mix":&models.DbItem{
				Title:       "Greens mix",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "base",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Cucumber cubes":&models.DbItem{
				Title:       "Cucumber cubes",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "veggies",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Tomatoes":&models.DbItem{
				Title:       "Tomatoes",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "veggies",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Goat cheese":&models.DbItem{
				Title:       "Goat cheese",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "protein",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Green Leaf Lettuce":&models.DbItem{
				Title:       "Green Leaf Lettuce",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "base",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Boiled quail eggs":&models.DbItem{
				Title:       "Boiled quail eggs",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "protein",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Roasted Chicken":&models.DbItem{
				Title:       "Roasted Chicken",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "protein",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},
			"Grated parmesan cheese":&models.DbItem{
				Title:       "Grated parmesan cheese",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "protein",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Carrot cubes":&models.DbItem{
				Title:       "Carrot cubes",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "veggies",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Brown rice":&models.DbItem{
				Title:       "Brown rice",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "base",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Roasted Sweet Potato":&models.DbItem{
				Title:       "Roasted Sweet Potato",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "veggies",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},
			"Apple cubes":&models.DbItem{
				Title:       "Apple cubes",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "dressings",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},

			"Almond":&models.DbItem{
				Title:       "Almond",
				Description: "Description",
				Price:       0,
				StorageTime: 12,
				Type:        "topping",
				Allergy:     "No allergy",
				ThrowSpeed:  0,
			},
		*/
	}
}
