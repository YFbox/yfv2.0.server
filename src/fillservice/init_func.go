package fillservice

import (
	"fmt"
	"server/fillservice/media/bowls_img"
	"server/fillservice/media/items_img"
	"server/models"
	"server/postgres"
)

func InitItems(items map[string]*models.DbItem) {
	fmt.Println("Start writing Items...")
	for _, i := range items {
		i.Picture, _ = items_img.Asset("item/" + i.Title + ".png")
		code, err := postgres.Db.InsertItem(*i)
		if err != nil {
			fmt.Println(err)
			return
		}
		i.Code = code
	}
	fmt.Println("Items recorded!")
}

func InitTrays(trays []int) {
	fmt.Println("Start writing trays...")
	for _, t := range trays {
		for i := 1; i < 12; i++ {
			postgres.Db.InsertTray(fmt.Sprintf("%04x", t+i), true)
		}
	}
	fmt.Println("Trays recorded!")
}

func InitBowls(bowls map[string]*models.DbBowl) {
	fmt.Println("Start writing bowls...")
	for name, b := range bowls {
		b.Picture, _ = bowls_img.Asset("bowl/" + name + ".png")
		_, err := postgres.Db.InsertBowl(*b)
		if err != nil {
			fmt.Println(err)
		}
	}
	fmt.Println("Bowls recorded!")
}
