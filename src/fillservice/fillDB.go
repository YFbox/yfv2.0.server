package fillservice

import (
	"fmt"
	"server/postgres"
)

func InitDatabase() {
	data, _ := Asset("sql/drop_db.sql")
	err := postgres.Db.InitFromBytes(data)
	if err != nil {
		fmt.Println(err)
		return
	} else {
		fmt.Println("Db dropped!")
	}

	data, _ = Asset("sql/init_db.sql")
	err = postgres.Db.InitFromBytes(data)
	if err != nil {
		fmt.Println(err)
		return
	} else {
		fmt.Println("New db created!")
	}

	trays := GetTrayList()
	InitTrays(trays)
	items := GetItemList()
	InitItems(items)
	bowls := GetBowlList(items)
	InitBowls(bowls)

	fmt.Println("New db filled!")
}
