module server

go 1.13

require (
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.3.0
	github.com/phf/go-queue v0.0.0-20170504031614-9abe38d0371d
	github.com/prometheus/common v0.10.0
	github.com/satori/go.uuid v1.2.0
	github.com/stianeikeland/go-rpio v4.2.0+incompatible
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	go.uber.org/zap v1.14.1
)
