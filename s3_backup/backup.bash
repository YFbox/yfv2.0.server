while :; do
  list="amount_bowls amount_items batch batch_cartridge cartridge daysession order_cartridge order_struct order_table recipe tray tray_batch"
  for x in $list
  do
  psql -d yf -c "select * from ${x}" > /home/yf/csv_backup/${x}.csv
  done
  q="select code,name,bowl_type,price,cooking_time,custom_flag,is_menu_flag,modified from bowl"
  psql -d yf -c "${q}" > /home/yf/csv_backup/bowl.csv
  q="select code,title,item_type,throw_speed,item_description,storage_time,allergy,price from item"
  psql -d yf -c "${q}" > /home/yf/csv_backup/item.csv
  
  ./venv/bin/python3 main.py
  sleep 86400  # once in 24 hours
done
