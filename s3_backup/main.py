#!/usr/bin/python3 -u

import boto3
import datetime
import json
import glob


def read_config():
    with open('config.json') as config_json:
        return json.loads(config_json.read())


def local_path_to_s3_path(local_path, path_prefix):
    return local_path[len(path_prefix):]


def upload_file(client, local_path, path_prefix):
    utc_time = datetime.datetime.utcnow()
    ts = utc_time.strftime('%Y_%m_%d__%H_%M')
    client.upload_file(local_path,
                       'history-data',
                       ts + '/' + local_path_to_s3_path(local_path, path_prefix))


def main():
    config_dict = read_config()
    session = boto3.session.Session()
    client = session.client(
        's3',
        region_name='fra1',
        endpoint_url='https://fra1.digitaloceanspaces.com',
        aws_access_key_id=config_dict['ACCESS_KEY'],
        aws_secret_access_key=config_dict['SECRET_KEY'])
    file_paths_to_upload = [filename for filename in glob.iglob(config_dict['LOCAL_PATH'] + '**/*.*', recursive=True)]
    for path in file_paths_to_upload:
        print('uploading ' + path)
        upload_file(client, path, path_prefix=config_dict['LOCAL_PATH'])
    print('done')


if __name__ == '__main__':
    main()
