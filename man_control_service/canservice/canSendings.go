package canservice

import (
	canproto "man/canservice/canProtoConfig"
)

func (man *CanManager) ManGetToPoint(posX uint16, posY uint16) {
	man.CanSend(uint32(canproto.MAN_COM_GET_TO_POINT), []uint8{uint8(posX), uint8(posX >> 8), uint8(posY), uint8(posY >> 8)}, 4, true, uint8(canproto.CAN_ID_MAN_STM))
}
func (man *CanManager) ManGetToItem(tray string) {
	man.CanSend(uint32(canproto.MAN_COM_GET_TO_ITEM), []uint8{uint8(StrHexToInt(tray[2:4])), uint8(StrHexToInt(tray[0:2]))}, 2, true, uint8(canproto.CAN_ID_MAN_STM))
}
func (man *CanManager) ManSetTestMode(on uint8) {
	man.CanSend(uint32(canproto.MAN_COM_TEST_MODE), []uint8{on}, 1, true, uint8(canproto.CAN_ID_MAN_STM))
}

func (man *CanManager) ManGetPoint() {
	man.CanSend(uint32(canproto.MAN_COM_GET_POINT), []uint8{}, 0, true, uint8(canproto.CAN_ID_MAN_STM))
}

func (man *CanManager) ManActuator(on uint8) {
	man.CanSend(uint32(canproto.MAN_COM_ACTUATOR), []uint8{on}, 1, true, uint8(canproto.CAN_ID_MAN_STM))
}

func (man *CanManager) ManUnload() {
	man.CanSend(uint32(canproto.MAN_COM_UNLOAD), []uint8{}, 0, true, uint8(canproto.CAN_ID_MAN_STM))
}
