package canservice

import (
	"github.com/tarm/serial"
)

// var logger, _ = zap.Config{
// 	Encoding:    "json",
// 	Level:       zap.NewAtomicLevelAt(zapcore.InfoLevel),
// 	OutputPaths: []string{"stdout", "../log/serial.log"},
// }.Build()

// var sugar = logger.Sugar()

type SerialConn struct {
	Port *serial.Port
	Buf  []byte
}

func (conn *SerialConn) Open(name string, baud int) error {
	var err error
	c := &serial.Config{Name: name, Baud: baud}
	conn.Port, err = serial.OpenPort(c)
	if err != nil {
		// sugar.Errorw("Serial failed connect to the port"+name,
		// 	"error", "SERIAL",
		// 	err, "time "+strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
		return err
	}
	conn.Buf = make([]byte, 27)
	// sugar.Infow("",
	// 	"info", "SERIAL",
	// 	"Serial connected to", name)
	return nil

}

func (conn *SerialConn) Write(data []byte) {
	_, err := conn.Port.Write(data)
	if err != nil {
		// sugar.Errorw("Serial can't write data",
		// 	"error", err,
		// 	"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
}

func (conn *SerialConn) Read() int {
	n, err := conn.Port.Read(conn.Buf)

	if err != nil {
		// sugar.Errorw("Serial can't read data",
		// 	"error", err,
		// 	"time", strconv.Itoa(time.Now().Hour())+":"+strconv.Itoa(time.Now().Minute()))
	}
	if n != 0 {
		// sugar.Infow("",
		// 	"info", "SERIAL",
		// 	"Serial read ", strconv.Itoa(n)+"bytes")
	}
	return n
}
