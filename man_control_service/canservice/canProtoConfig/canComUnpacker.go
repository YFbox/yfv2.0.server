package canproto

const (
	/*
	   UNPACKER MSG ADDR
	*/

	/***COMMAND ADDR***/ ////////////////////////////////////////////////////////////////////////////////////////////

	//1 byte data
	//#0 - reserved by CAN
	UNPACKER_COM_NET_INIT = 0x001

	//2 byte data
	//#0 - reserved by CAN
	//#1 - ON/OFF
	UNPACKER_COM_TEST_MODE = 0x002

	//4 byte data
	//#0 - reserved by CAN
	//#1#2 - 	bowl id
	//#3 - 		amount items
	UNPACKER_COM_ADD_BOWL = 0x003

	//7 byte data
	//#0 - reserved by CAN
	//#1#2 - 	bowl id
	//#3#4 - 	tray
	//#5   -  amount
	//#6 - 		speed of throwing
	UNPACKER_COM_ADD_ITEM = 0x004

	//2 byte data
	//#0 - reserved by CAN
	//#1 - amount
	UNPACKER_COM_PICKUP_ITEM = 0x005

	//1 byte data
	//#0 - reserved by CAN
	UNPACKER_COM_GET_STATE = 0x006

	/***TEST COMMANDS ADDR***/ ////////////////////////////////////////////////////////////////////////////////////////////

	//2 byte data
	//#0 - reserved by CAN
	//#1 - angle
	UNPACKER_COM_TURN_GRIPPER = 0x007

	//2 byte data
	//#0 - reserved by CAN
	//#1 - angle
	UNPACKER_COM_TURN_ARM = 0x008

	//2 byte data
	//#0 - reserved by CAN
	//#1 - angle
	UNPACKER_COM_TURN_FLAG = 0x009

	//2 byte data
	//#0 - reserved by CAN
	//#1 - angle
	UNPACKER_COM_TURN_SNAKE = 0x00A

	//2 byte data
	//#0 - reserved by CAN
	//#1 - on(1)/off(0)
	UNPACKER_COM_SUCKER = 0x00B

	//4 byte data
	//#0 - reserved by CAN
	//#1#2 - pos
	//#3 - direction
	UNPACKER_COM_STEPPER = 0x00C

	//2 byte data
	//#0 - reserved by CAN
	//#1 - direction
	UNPACKER_COM_CALIBRATION = 0x00D

	//1 byte data
	//#0 - reserved by CAN
	UNPACKER_COM_CYCLE = 0x00E

	/***REPLY ADDR***/ /////////////////////////////////////////////////////////////////////////////////////////////////////

	//1 byte data
	//#0 - reserved by CAN
	UNPACKER_REPLY_STATE = 0x00F

	//1 byte data
	//#0 - reserved by CAN
	UNPACKER_REPLY_CONNECTED = 0x010

	//2 byte data
	//#0 - reserved by CAN
	//#1 - 	amount ready
	UNPACKER_REPLY_PROGRESS = 0x011

	//4 byte data
	//#0 - reserved by CAN
	//#1#2 - 	bowl id
	//#3 -	 	count_loaded
	UNPACKER_REPLY_STATE_LOADED = 0x012

	//3 byte data
	//#0 - reserved by CAN
	//#1#2 - 	bowl id
	UNPACKER_REPLY_STATE_COOKING = 0x013

	//1 byte data
	//#0 - reserved by CAN
	UNPACKER_REPLY_STATE_PLATE_OUT = 0x014

	//1 byte data
	//#0 - reserved by CAN
	UNPACKER_REPLY_STATE_PLATE_ON = 0x015
)
