package canproto

const (

	//2 byte data
	//#0 - confirmation id
	//#1 on/off
	CONV_PUMP = BASE_CAN_ADDR_CONV_STM + 0x003

	//3 bytes data
	//#0 - confirmation id
	//#1 - module source
	//#2- module id
	CONV_COM_PROCESS_ITEM = BASE_CAN_ADDR_CONV_STM + 0x004
)
