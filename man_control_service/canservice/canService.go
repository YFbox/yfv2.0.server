package canservice

import (
	"container/list"
	"errors"
	"fmt"
	"log"
	canproto "man/canservice/canProtoConfig"
	"strings"
	"sync"
	"time"
)

func NewCanManager(port string) *CanManager {
	s := SerialConn{}
	connErr := s.Open(port, 1000000)

	//cmLogger.LogInfo(fmt.Sprintf("Serial port %s opened!", port))

	mu := &sync.Mutex{}
	cm := &CanManager{
		SerialConn:   s,
		RxMsg:        make(chan *RCanMsg),
		Mutex:        mu,
		SendQueue:    list.New(),
		RxMsgMap:     make(map[uint32]uint8),
		TxMsgMap:     make(map[uint32]uint8),
		Source_id:    canproto.CAN_ID_PC,
		Check_target: true,
	}
	if connErr != nil {
		cm.ConnErr = connErr
		//cmLogger.LogError(errors.New("Can't open serial port"))
		//return nil
	}

	cm.Port.baud = "B"
	cm.Port.channel = "1"
	cm.CloseFlag = false
	return cm
}

func (man *CanManager) Listen() {
	//cmLogger.LogInfo("Can manager started")
	if man.ConnErr == nil {
		err := !man.Connect()
		if err {
			return
		}
		for !man.CloseFlag {
			n := man.SerialConn.Read()
			if n > 5 {
				s := string(man.SerialConn.Buf)
				strings.Trim(s, "\r\n")
				msg, _ := ParseMsg(s)
				if msg != nil && msg.Dlc > 0 {
					if !man.CheckConfirmation(msg) && man.CanCheckTarget(msg) {
						msgMarker := msg.Data[0] >> 1 & 1
						if man.CanMapIsNewId(msg.Id, msgMarker) {
							log.Println(fmt.Sprintf("Got message: Id(%d), DLC (%d), Data (%s)", msg.Id, msg.Dlc, msg.Data))
							man.RxMsg <- msg
						}
						man.SendConfirmation(msg)
					}
				}
			}
			time.Sleep(time.Microsecond)
		}
	}
}

func ParseMsg(data string) (*RCanMsg, error) {
	defer func() {
		if r := recover(); r != nil {
			//cmLogger.LogError(errors.New("Can't parse message"))
		}
	}()
	idx := uint8(strings.Index(data, "t1"))
	msg := &RCanMsg{}
	msg.Id = uint32(StrHexToInt(data[idx+2 : idx+5]))
	if msg.Id == 0 {
		return nil, errors.New("WrongId")
	}
	msg.Dlc = uint8(StrHexToInt(data[idx+5 : idx+6]))
	for i := uint8(0); i < msg.Dlc; i++ {
		msg.Data[i] += uint8(StrHexToInt(data[idx+6+2*i : idx+6+(i+1)*2]))
	}
	return msg, nil
}

func (man *CanManager) CheckConfirmation(msg *RCanMsg) bool {
	flag := msg.Data[0] & 1
	if flag > 0 {
		if man.Sending_flag && msg.Id == uint32(StrHexToInt(man.TxMsg.Id)) {
			id := msg.Data[0] >> 2
			if id == man.Target_id {
				man.Is_confirmed = true
			}
		}
		return true
	}
	return false
}

func (man *CanManager) SendConfirmation(msg *RCanMsg) {
	msg_id := uint8(1)               //0bit confirmation bit
	msg_id |= msg.Data[0] & (1 << 1) //1bit msg marker
	msg_id |= man.Source_id << 2     //2-7bit source id
	sdata := fmt.Sprintf("%02x", msg_id)
	if msg.Dlc > 1 {
		for i := uint8(1); i < msg.Dlc; i++ {
			sdata += fmt.Sprintf("%02x", msg.Data[i])
		}
	}
	cmsg := TCanMsg{
		Id:   fmt.Sprintf("%03x", msg.Id),
		Dlc:  fmt.Sprintf("%01x", msg.Dlc),
		Data: sdata,
	}
	man.SendMsg(cmsg)
}

func (man *CanManager) CanMapIsNewId(key uint32, id uint8) bool {
	if val, ok := man.RxMsgMap[key]; ok {
		if val == id {
			return false
		}
	}
	man.RxMsgMap[key] = id
	return true
}
func (man *CanManager) CanCheckTarget(msg *RCanMsg) bool {
	id := msg.Data[0] >> 2 //2-7bit target id
	if id == man.Source_id || !man.Check_target {
		return true
	}
	return false
}
func (man *CanManager) CanMapGetNewId(key uint32) uint8 {
	if val, ok := man.TxMsgMap[key]; ok {
		if val == 1 {
			man.TxMsgMap[key] = 0
		} else {
			man.TxMsgMap[key] = 1
		}
	} else {
		man.TxMsgMap[key] = 0
	}
	return man.TxMsgMap[key]
}
func (man *CanManager) Connect() bool {
	msg := "S" + man.Port.channel + man.Port.baud + "\r\n"
	man.SerialConn.Write([]byte(msg))

	msg = "O" + man.Port.channel + "0\r\n"
	man.SerialConn.Write([]byte(msg))

	//cmLogger.LogInfo("Connected to CAN bus")

	return true
}

func (man *CanManager) CanSend(addr uint32, data []uint8, count uint8, need_confirm bool, target_id uint8) {
	if count < 8 {
		msg_id := (man.CanMapGetNewId(addr) & 1) << 1 //msg marker
		msg_id |= target_id << 2                      //target id
		sdata := fmt.Sprintf("%02x", msg_id)
		if count > 0 {
			for i := uint8(0); i < count; i++ {
				sdata += fmt.Sprintf("%02x", data[i])
			}
		}
		Cmsg := CTCanMsg{
			msg: TCanMsg{
				Dlc:  fmt.Sprintf("%01x", count+1),
				Id:   fmt.Sprintf("%03x", addr),
				Data: sdata,
			},
			target_id: target_id,
		}
		if need_confirm {
			man.SendQueue.PushBack(Cmsg)
		} else {
			man.SendMsg(Cmsg.msg)
		}
	}
}
func (man *CanManager) CanSending() {
	for !man.CloseFlag && !man.Error_flag {
		if man.Sending_flag {
			for iter := 0; !man.Is_confirmed; iter++ {
				if iter == SENDING_TIMEOUT {
					if man.Sending_attempts < SENDING_ATTEMPTS {
						man.Sending_attempts++
						iter = 0
						man.SendMsg(man.TxMsg)
					} else {
						man.Error_flag = true
						break
					}
				}
				time.Sleep(1 * time.Millisecond)
			}
			man.Sending_flag = false
			man.Is_confirmed = false
			man.Sending_attempts = 0
			man.Target_id = 0
			man.TxMsg = TCanMsg{}
		} else {
			if man.SendQueue.Len() > 0 {
				Cmsg := man.SendQueue.Remove(man.SendQueue.Front()).(CTCanMsg)
				man.TxMsg = Cmsg.msg
				man.SendMsg(man.TxMsg)
				man.Sending_flag = true
				man.Is_confirmed = false
				man.Sending_attempts++
				man.Target_id = Cmsg.target_id
			}
		}
		time.Sleep(500 * time.Millisecond)
	}
}

func (man *CanManager) SendMsg(canMsg TCanMsg) bool {
	msg := "t" + man.Port.channel + canMsg.Id + canMsg.Dlc + canMsg.Data + "\r\n"
	//	log.Println(fmt.Sprintf("Send to CAN bus: msg (%s)", msg))
	if man.ConnErr == nil {
		man.Mutex.Lock()
		man.SerialConn.Write([]byte(msg))
		man.Mutex.Unlock()
	}
	return true
}

func StrHexToInt(s string) int {
	val := 0
	s = strings.ToLower(s)
	for i := 0; i < len(s); i++ {
		tmpval := 0
		switch s[i] {
		case '0':
			tmpval = 0
		case '1':
			tmpval = 1
		case '2':
			tmpval = 2
		case '3':
			tmpval = 3
		case '4':
			tmpval = 4
		case '5':
			tmpval = 5
		case '6':
			tmpval = 6
		case '7':
			tmpval = 7
		case '8':
			tmpval = 8
		case '9':
			tmpval = 9
		case 'a':
			tmpval = 10
		case 'b':
			tmpval = 11
		case 'c':
			tmpval = 12
		case 'd':
			tmpval = 13
		case 'e':
			tmpval = 14
		case 'f':
			tmpval = 15
		}
		st := len(s) - i - 1
		val += tmpval << (st * 4)
	}
	return val
}
