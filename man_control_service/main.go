package main

import (
	"errors"
	"fmt"
	"log"
	"man/canservice"
	canproto "man/canservice/canProtoConfig"
	"time"
)

const (
	CanPort = "/dev/ttyACM0" ///origin
)

type Man struct {
	PosX  int
	PosY  int
	State bool
}

var m Man
var close, gotflag bool
var can *canservice.CanManager

func main() {
	close = false
	can = canservice.NewCanManager(CanPort)
	if can == nil {
		log.Println(errors.New("Can't init can service"))
		return
	}
	go can.Listen()
	go can.CanSending()
	go Process()
	can.ManSetTestMode(1)
	for !close {
		var com int
		fmt.Println("1.Get to pos")
		fmt.Println("2.Get to tray")
		fmt.Println("3.Show pos")
		fmt.Println("4.Set actuator state")
		fmt.Println("5.Unload")
		fmt.Println("6.Exit")
		fmt.Scanf("%d\n", &com)
		switch com {
		case 1:
			var pos int
			fmt.Println("Enter posX (A,B,C,D)")
			fmt.Scanf("%04x\n", &pos)
			m.PosX = pos
			fmt.Println("Enter posY (1,2,3,4)")
			fmt.Scanf("%04x\n", &pos)
			m.PosY = pos
			flag := ""
			fmt.Println("Confirm? (y/n)")
			fmt.Scanf("%s\n", &flag)
			if flag == "y" {
				can.ManGetToPoint(uint16(m.PosX), uint16(m.PosY))
				fmt.Println(fmt.Sprintf("Get to \nposX = %04x\nposY = %04x ", m.PosX, m.PosY))
			}
			break
		case 2:
			var s string
			fmt.Println("Enter tray")
			fmt.Scanf("%s\n", &s)
			flag := ""
			fmt.Println("Confirm? (y/n)")
			fmt.Scanf("%s\n", &flag)
			if flag == "y" {
				fmt.Println("Get to tray " + s)
				can.ManGetToItem(s)
			}
			break
		case 3:
			for !gotflag {
				can.ManGetPoint()
				time.Sleep(500 * time.Millisecond)
			}
			gotflag = false
			break

		case 4:
			var on uint8
			fmt.Println("Enter actuator state (1/0)")
			fmt.Scanf("%d\n", &on)
			if on == 1 || on == 0 {
				can.ManActuator(on)
			}
			break
		case 5:
			flag := ""
			fmt.Println("Confirm? (y/n)")
			fmt.Scanf("%s\n", &flag)
			if flag == "y" {
				fmt.Println("Unloading..")
				can.ManUnload()
			}
			break
		case 6:
			close = true
			break
		}
	}
	can.ManSetTestMode(0)
	time.Sleep(3 * time.Second)
	can.CloseFlag = true
}

func Process() {
	for !close {
		msg := <-can.RxMsg
		if msg.Dlc > 0 {
			switch msg.Id {
			case canproto.MAN_REPLY_POINT:
				if msg.Dlc == 5 {
					gotflag = true
					m.PosX = (int(msg.Data[2]) << 8) + int(msg.Data[1])
					m.PosY = (int(msg.Data[4]) << 8) + int(msg.Data[3])
					fmt.Println(fmt.Sprintf("posX = %04x\nposY = %04x ", m.PosX, m.PosY))
				}
				break
			}

		}
	}
}
